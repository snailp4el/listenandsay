package com.snaip4el.listenandsay;
import android.content.Context;
import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


//проверка выборки из базы по критериям

public class AllCriteriasFetchTest {
    private static final String TAG = "AllCriteriasFetchTest";
    private BasePhraseDao userDao;
    private BaseDB db;
    Context context;

    @Before
    public void createDb() {
        System.out.println("############################");
        System.out.println("@Before");
        context = InstrumentationRegistry.getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, BaseDB.class).build();
        userDao = db.getPhraseDao();
    }

    @Test
    public void writeUserAndReadInList() throws Exception {
        System.out.println("############################");
        System.out.println("@Test");

        List<BasePhrase> pa = new ArrayList<>();
        //GETTING PHRASE FROM BASE
        BaseDB db = Room.databaseBuilder(context, BaseDB.class, "populus-database").build();
        BasePhraseDao phraseDao = db.getPhraseDao();



        List <ListeningResult> listeningResults = new ArrayList<>();
        List <SpeakingResult> speakingResults =new ArrayList<>();

        listeningResults.add(ListeningResult.DID_NOT_PRESS);
        //listeningResults.add(ListeningResult.PRESSED_NO);
        //listeningResults.add(ListeningResult.PRESSED_YES);

        speakingResults.add(SpeakingResult.DID_NOT_SAY);
        //speakingResults.add(SpeakingResult.SAID_OK_FIRST_TRY);
        //speakingResults.add(SpeakingResult.SAID_OK);
        speakingResults.add(SpeakingResult.SAID_WRONG);


        //pa = phraseDao.getAllPhrase(NamesSR.getLerningLanguage(getApplicationContext()));
        pa = phraseDao.getAllPhraseListeningAndSpeakingResult(NamesSR.getLerningLanguage(context), listeningResults, speakingResults);

        int i = phraseDao.countZerroDifficultyPhrases(NamesSR.getLerningLanguage(context));

        for(BasePhrase bp: pa){
            System.out.println(bp.getPhrase() +" - " + bp.getPhraseDifficulty());
        }

        Map<String, Integer> countRsults= new HashMap<>();
        countRsults.put("DID_NOT_PRESS",0);
        countRsults.put("PRESSED_NO",0);
        countRsults.put("PRESSED_YES",0);

        countRsults.put("DID_NOT_SAY",0);
        countRsults.put("SAID_OK_FIRST_TRY",0);
        countRsults.put("SAID_OK",0);
        countRsults.put("SAID_WRONG",0);




        for (BasePhrase bp: pa){
            if (bp.listeningResult == ListeningResult.DID_NOT_PRESS) countRsults.put("DID_NOT_PRESS",countRsults.get("DID_NOT_PRESS")+1);
            if (bp.listeningResult == ListeningResult.PRESSED_NO) countRsults.put("PRESSED_NO",countRsults.get("PRESSED_NO")+1);
            if (bp.listeningResult == ListeningResult.PRESSED_YES) countRsults.put("PRESSED_YES",countRsults.get("PRESSED_YES")+1);

            if (bp.speakingResult == SpeakingResult.DID_NOT_SAY) countRsults.put("DID_NOT_SAY",countRsults.get("DID_NOT_SAY")+1);
            if (bp.speakingResult == SpeakingResult.SAID_OK_FIRST_TRY) countRsults.put("SAID_OK_FIRST_TRY",countRsults.get("SAID_OK_FIRST_TRY")+1);
            if (bp.speakingResult == SpeakingResult.SAID_OK) countRsults.put("SAID_OK",countRsults.get("SAID_OK")+1);
            if (bp.speakingResult == SpeakingResult.SAID_WRONG) countRsults.put("SAID_WRONG",countRsults.get("SAID_WRONG")+1);
            //System.out.println(bp);
        }

        //System.out.println(countRsults);//bases criterias
        System.out.println("COUNT " + pa.size());

    }

    @After
    public void closeDb() throws IOException {
        System.out.println("############################");
        System.out.println("@After");
        db.close();
    }


}
