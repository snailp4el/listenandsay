package com.snaip4el.listenandsay;

import android.content.Context;
import androidx.room.Room;
import androidx.test.InstrumentationRegistry;
import androidx.test.runner.AndroidJUnit4;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@RunWith(AndroidJUnit4.class)
public class SimpleEntityReadWriteTest {
    private static final String TAG = "SimpleEntityReadWriteTest";
    private BasePhraseDao userDao;
    private BaseDB db;
    Context context;

    @Before
    public void createDb() {
        System.out.println("############################");
        System.out.println("@Before");
        context = InstrumentationRegistry.getTargetContext();
        db = Room.inMemoryDatabaseBuilder(context, BaseDB.class).build();
        userDao = db.getPhraseDao();
    }

    @Test
    public void writeUserAndReadInList() throws Exception {
        System.out.println("############################");
        System.out.println("@Test");

        List<BasePhrase> pa = new ArrayList<>();
        //GETTING PHRASE FROM BASE
        BaseDB db = Room.databaseBuilder(context, BaseDB.class, "populus-database").build();
        BasePhraseDao phraseDao = db.getPhraseDao();

        List<BasePhrase> phrases;
        FetchCriterias fetchCriterias = FetchCriterias.getInstance(context);
        List <ListeningResult> results = fetchCriterias.getListeningResultsForQuery();
        for (ListeningResult lr: results){
            System.out.println(lr.toString());
        }


        //pa = phraseDao.getAllPhrase(NamesSR.getLerningLanguage(getApplicationContext()));
        pa = phraseDao.getAllPhraseListeningResult(NamesSR.getLerningLanguage(context), results);


        for (BasePhrase bp: pa){
            System.out.println(bp.toString());
        }
    }

    @After
    public void closeDb() throws IOException {
        System.out.println("############################");
        System.out.println("@After");
        db.close();
    }


}
