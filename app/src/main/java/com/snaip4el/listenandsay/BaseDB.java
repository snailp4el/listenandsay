package com.snaip4el.listenandsay;
import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {BasePhrase.class,}, version = 1, exportSchema = false)
public abstract class BaseDB extends RoomDatabase {
    public abstract BasePhraseDao getPhraseDao();
}