package com.snaip4el.listenandsay;

import java.util.HashMap;
import java.util.Map;

public class Languages {

    public static final Map<String, String> tatoebaToGoole = new HashMap<String, String>()
    {
        {
            put("aze" , "az");
            put("ind" , "id");
            put("zsm" , "ms");
            put("bre" , "br");
            put("cat" , "ca");
            put("cym" , "cy");
            put("dan" , "da");
            put("deu" , "de");
            put("est" , "et");
            put("eng" , "en");
            put("spa" , "es");
            put("epo" , "eo");
            put("eus" , "eu");
            put("fra" , "fr");
            put("fry" , "fy");
            put("glg" , "gl");
            put("ina" , "ia");
            put("ita" , "it");
            put("lat" , "la");
            put("lit" , "lt");
            put("jbo" , "jbo");
            put("hun" , "hu");
            put("nld" , "nl");
            put("oci" , "oc");
            put("uzb" , "uz");
            put("nds" , "nds");
            put("pol" , "pl");
            put("por" , "pt-BR");
            put("ron" , "ro");
            put("fin" , "fi");
            put("swe" , "sv");
            put("tgl" , "tl");
            put("kab" , "kab");
            put("vie" , "vi");
            put("tur" , "tr");
            put("ces" , "cs");
            put("ell" , "el");
            put("bel" , "be");
            put("rus" , "ru");
            put("ukr" , "uk");
            put("mar" , "mr");
            put("hin" , "hi");
            put("ben" , "bn");
            put("kat" , "ka");
            put("cmn" , "zh-Hans");
            put("jpn" , "ja");
            put("kor" , "ko");
            put("ara" , "ar");
        }
    };






}
