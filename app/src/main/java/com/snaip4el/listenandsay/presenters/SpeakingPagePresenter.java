package com.snaip4el.listenandsay.presenters;

import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.ToggleButton;
import com.snaip4el.listenandsay.AnswerTextAnalyzer;
import com.snaip4el.listenandsay.Phrase;
import com.snaip4el.listenandsay.Player;
import com.snaip4el.listenandsay.Players;
import com.snaip4el.listenandsay.SpeechRecognizerSingleton;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;
import java.util.List;

public class SpeakingPagePresenter extends TaskPresenter implements SpeechRecognizerSingleton.IRecognizersNeeds {

    SpeechRecognizerSingleton speechRecognizer;
    private String TAG = "SpeakingPagePresenter";
    IspeakingPageExtra pageExtra;
    ToggleButton tbChecked;

    public SpeakingPagePresenter(Phrase phrase) {
        super(phrase);
        this.pageExtra = pageExtra;
    }

    @Override
    public void playButtonIsPressed() {
        Log.i(TAG, "playButtonIsPressed " + tbChecked.isChecked());
    }

    @Override
    public void onPageApperListner() {
        Log.i("comeGo", "come++"+ phrase.getBasePhrase().toString());

        speechRecognizer = SpeechRecognizerSingleton.getInstance(this);
        setInitialTexts();
        tbChecked = (ToggleButton) itaskPage.getPlayButton();

        tbChecked.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (phrase.getByteSound() == null){
                tbChecked.setChecked(false);
            }
            if (isChecked){
                    playPhrase(50);
            }else {
                stopAllActivitys();
            }
        });
        tbChecked.setChecked(true);


    }

    @Override
    public void onPageDisapperListner() {
        Log.i("comeGo", "phrase++"+ phrase.getPhrase());
        Log.i("comeGo", "go++"+ phrase.getBasePhrase().toString());
        Log.i("comeGo", "++++++++++++++++++++++++++++++++++++++++++++");
        stopAllActivitys();
        try {
            tbChecked.setChecked(false);
        }catch (Exception e){

        }
        updatePhrase();
    }

    private void stopAllActivitys(){
        try {
            Player.stopPlay();
            speechRecognizer.stopListenig();
            ttsStop();
        }catch (Exception e){
        }
    }

    @Override
    public void stopPlayListner(Players player) {
        Log.i(TAG,"stopPlayListner " + player.name());
        if (player == Players.TTS){//todo delete
            Log.i(TAG,"play text");
        }
        pageExtra.runInUiThread(() -> speechRecognizer.startListening(player));
        //new Handler().post(() -> speechRecognizer.startListening(player));
    }



    @Override
    public void OnStopRecognition(List<String> resultOfRecognition, Players playerWhasUsed) {
        Log.i(TAG, "OnStopRecognition");
        setPhraseInformation();
        tbChecked.setChecked(false);
        String lst = "";
        if(playerWhasUsed == Players.AUDIO) lst = phrase.getPhrase();
        if (playerWhasUsed == Players.TTS) lst = ttsGetLastSaid();

        AnswerTextAnalyzer.AnswerAnalisysResult result = AnswerTextAnalyzer.ANALYZE(resultOfRecognition, playerWhasUsed, lst);
        Log.i(TAG, result.toString());

        //set speakingResult
        if (result.saidCorrectly != true){
            phrase.getBasePhrase().setSpeakingResult(SpeakingResult.SAID_WRONG);
            if (playerWhasUsed == Players.AUDIO) phrase.setnOfTryPulusOne();
        }else {
            if(phrase.getnOfTry() == 1){
                phrase.getBasePhrase().setSpeakingResult(SpeakingResult.SAID_OK_FIRST_TRY);
            }else {
                phrase.getBasePhrase().setSpeakingResult(SpeakingResult.SAID_OK);
            }
        }
        // don't allow repeat words many times
        if (result.saidCorrectly != true && playerWhasUsed == Players.TTS){
            phrase.setnWordTry(phrase.getnWordTry()+1);
        }else {
            phrase.setnWordTry(0);
        }

        //what to do after speaking
        if(result.saidCorrectly == true && playerWhasUsed == Players.AUDIO) {
            itaskPage.nextPage();
            return;
        }
        if(result.saidCorrectly != true && playerWhasUsed == Players.AUDIO) {
            ttsPlay(result.wrongestWord);
            return;
        }
        if(result.saidCorrectly == true && playerWhasUsed == Players.TTS) {
            playPhrase(50);
            return;
        }
        if(playerWhasUsed == Players.TTS && phrase.getnWordTry() >= 3) {
            playPhrase(50);
            return;
        }
        if(result.saidCorrectly == false && playerWhasUsed == Players.TTS) {
            ttsPlay(result.wrongestWord);
            return;
        }

    }

    @Override
    public void OnRecognitionError(int Nerror) {
        tbChecked.setChecked(false);
    }

    @Override
    public void cantPlay() {
        tbChecked.setChecked(false);
        //showATost(itaskPage.getContx().getResources().getString(R.string.loading));
        new Handler().postDelayed(()-> tbChecked.setChecked(true), 500);
    }

    @Override
    public void setMicOpasity(float num) {
        pageExtra.SetMicOpacity(num);
    }



    @Override
    public Context getContext() {
        return itaskPage.getContx();
    }

    //mast be implement in speakingpage to access to its fields;
    public interface IspeakingPageExtra {
        void SetMicOpacity(float num);
        void runInUiThread(Runnable r);
    }

    @Override
    public void setPagExtra(Object pageExtraObject) {
        pageExtra = (IspeakingPageExtra) pageExtraObject;
    }

}
