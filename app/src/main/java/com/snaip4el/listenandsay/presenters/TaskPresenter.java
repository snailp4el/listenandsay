package com.snaip4el.listenandsay.presenters;

import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import androidx.room.Room;
import com.snaip4el.listenandsay.Anki.AnkiSender;
import com.snaip4el.listenandsay.Anki.FileWorker;
import com.snaip4el.listenandsay.BaseDB;
import com.snaip4el.listenandsay.BasePhraseDao;
import com.snaip4el.listenandsay.IPlayersActions;
import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.Phrase;
import com.snaip4el.listenandsay.Player;
import com.snaip4el.listenandsay.Players;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TatoebaFetcher;
import com.snaip4el.listenandsay.UI.ItaskPage;
import com.snaip4el.listenandsay.TTSplayer;

import java.io.File;

public abstract class TaskPresenter implements IPlayersActions {
    ItaskPage itaskPage;
    Phrase phrase;
    String TAG = "TaskPresenter";
    private TTSplayer ttSplayer;
    private String lastSeadTTS;

    //BUTTONS IS PRESSED
    public abstract void playButtonIsPressed();
    public abstract void onPageApperListner();
    public abstract void onPageDisapperListner();
    public abstract void stopPlayListner(Players players);
    public abstract void cantPlay();
    public abstract void setPagExtra(Object pageExtraObject);

    public TaskPresenter(Phrase phrase) {
        this.phrase = phrase;
    }

    //page on create
    public void setItaskPage(ItaskPage itaskPage) {
        this.itaskPage = itaskPage;
        downloadStuffToPhrase();
        ttSplayer = TTSplayer.getInstance(NamesSR.GET_LEARNING_LANGUAGE_LOCALE(itaskPage.getContx()),this, itaskPage.getContx());
        switch (phrase.getBasePhrase().listeningResult){
            case PRESSED_YES:
                buttonSwicher(true);
                break;
            case PRESSED_NO:
                buttonSwicher(false);
                break;
        }

    }

    public void toAnkiButtonIsPressed(){
        Log.i(TAG, "toAnkiButtonIsPressed()");
        AnkiSender.getInstens(itaskPage.getContx()).addCardsToAnkiDroid(phrase, itaskPage.getContx());
        setndMp3ToAnki();
    }

    public void unerstendButtonPressed() {
        Log.i(TAG, "unerstendButtonPressed");
        buttonSwicher(true);
        phrase.getBasePhrase().listeningResult = ListeningResult.PRESSED_YES;
        setPhraseInformation();
        //testTtsPlay();

    }

    public void dontUnderstendButtonPressed() {
        Log.i(TAG,"dontUnderstendButton");
        buttonSwicher(false);
        phrase.getBasePhrase().listeningResult = ListeningResult.PRESSED_NO;
        setPhraseInformation();
        //testTtsPlay();
    }

    private void testTtsPlay(){
        ttSplayer.ttsSayText(phrase.getPhrase());
    }

    protected void setInitialTexts(){
        Log.i(TAG, "setInitialTexts");
        itaskPage.getEnAnsverTV().setText(""+phrase.getBasePhrase().id);
        itaskPage.getTranslationTV().setText(""+phrase.getBasePhrase().id);
    }

    protected void setText(){
        try{
            itaskPage.getEnAnsverTV().setText(phrase.getPhrase());
            itaskPage.getTranslationTV().setText(phrase.getTranslation().get(0));
            itaskPage.getLinkTV().setText(phrase.getGoogleTranslation());
        }catch (Exception e){

        }
    }

    protected void playPhrase(int delay){
        new Handler().postDelayed(() -> Player.playSound(phrase, itaskPage.getContx(),this), delay);
    }

    protected void stopPlayPhrase(){
        Player.stopPlay();
    }

    protected void ttsPlay(String text){
        lastSeadTTS = text;
        ttSplayer.ttsSayText(text);
    }

    protected void ttsStop(){
        ttSplayer.ttsStop();
    }
    protected String ttsGetLastSaid(){
        return lastSeadTTS;
    }

    protected void downloadStuffToPhrase(){
        new TatoebaFetcher.DownloadSoundToPhrase().execute(phrase);
        new TatoebaFetcher.AddTextToPhrase(phrase, itaskPage.getContx()).execute();
    }

    protected void buttonSwicher(boolean understood){
        // make oposite button unactive
        itaskPage.getUnderstandButton().setEnabled(!understood);
        itaskPage.getDontUnderstandButton().setEnabled(understood);
    }
    protected void setPhraseInformation(){
        itaskPage.getEnAnsverTV().setText(phrase.getPhrase());
        itaskPage.getTranslationTV().setText(phrase.getGoogleTranslation());
    }

    protected void updatePhrase(){

        BaseDB db = Room.databaseBuilder(itaskPage.getContx(), BaseDB.class, "populus-database").build();
        final BasePhraseDao dao = db.getPhraseDao();
        AsyncTask.execute(() -> dao.updatePhrase(phrase.getBasePhrase()));
    }

    public void setndMp3ToAnki(){
        new Handler().post(() -> {
            File phraseFile = new File(AnkiSender.getPhrasePath(itaskPage.getExternalFilesDirs()) + "/"+phrase.getId() +".mp3");
            boolean b = FileWorker.SAVE_FILE(phraseFile, phrase);
        });
    }

}
