package com.snaip4el.listenandsay.presenters;

import android.util.Log;
import com.snaip4el.listenandsay.Phrase;
import com.snaip4el.listenandsay.Player;
import com.snaip4el.listenandsay.Players;

public class ListeningPagePresenter extends TaskPresenter{

    private static final String TAG = "ListeningPagePresenter";

    public ListeningPagePresenter(Phrase phrase) {
        super(phrase);
    }






    @Override
    public void playButtonIsPressed() {
        Log.i(TAG, "playButton()");
        setText();
        Player.playSound(phrase, itaskPage.getContx(), this);
    }

    @Override
    public void onPageApperListner() {
        Log.i(TAG, "onPageApperListner");
        setInitialTexts();
        playPhrase(200);
    }

    @Override
    public void onPageDisapperListner() {
        updatePhrase();
    }

    @Override
    public void stopPlayListner(Players players) {

    }

    @Override
    public void cantPlay() {

    }


    @Override
    public void setPagExtra(Object pageExtraObject) {
    }

}
