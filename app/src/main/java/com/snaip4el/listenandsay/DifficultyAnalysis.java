package com.snaip4el.listenandsay;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import androidx.room.Room;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
//

public class DifficultyAnalysis extends AsyncTask<Void, Void, Integer> {
    private static String TAG = "СomplexityAnalysis";
    private Context context;
    protected ProgressDialog dialog;
    private static final int loadingDifficultyTrashHold = 99;

    public DifficultyAnalysis(Context context) {
        this.context = context;
        dialog = new ProgressDialog(context);

    }

    public List<BasePhrase> CALCULATE_PHRASE_DIFICLTY(List<BasePhrase> basePhrases){
        Map<String, Integer> countedWord = new HashMap<>();

        for (BasePhrase bp : basePhrases){

            for (String word: CLEAN_AND_SPLIT(bp.getPhrase())){
                if (!countedWord.containsKey(word)){
                    countedWord.put(word, 1);
                }else {
                    countedWord.put(word, countedWord.get(word)+ 1);
                }
            }
        }

        List sortedList = sortByComparator(countedWord, false);

        for (BasePhrase bp : basePhrases){
            List<String> splitedPhrase = CLEAN_AND_SPLIT(bp.getPhrase());
            long difficultySumm = 0;
            for (String word: splitedPhrase){
                difficultySumm = difficultySumm + sortedList.indexOf(word);
            }
            if(splitedPhrase.size() != 0){
                bp.setPhraseDifficulty((int)difficultySumm/splitedPhrase.size());
            }

            Log.i(TAG,bp.getPhrase() +"-" + bp.getPhraseDifficulty());
        }

        return basePhrases;
    }

    public static String CLEAN_TEXT(String s){
        s = s.toLowerCase();
        //s = s.replaceAll("  ", " ");
        s = s.replaceAll("[^'abcdefghijklmnopqrstuvwxyz ]", "");
        s = s.replaceAll("\\s+", " ");

        return s;
    }

    public static List<String> CLEAN_AND_SPLIT(String s){
        s = CLEAN_TEXT(s);
        List<String> splited = new ArrayList<>();
        for(String word: s.split(" ")){
            if(word.length()>0)splited.add(word);
        }
        return splited;
    }

    private static List<String> sortByComparator(Map<String, Integer> unsortMap, final boolean order)
    {

        List<Map.Entry<String, Integer>> list = new LinkedList<Map.Entry<String, Integer>>(unsortMap.entrySet());

        // Sorting the list based on values
        Collections.sort(list, new Comparator<Map.Entry<String, Integer>>()
        {
            public int compare(Map.Entry<String, Integer> o1,
                               Map.Entry<String, Integer> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
       ArrayList<String> fin = new ArrayList();
        for (Map.Entry<String, Integer> entry : list)
        {
            fin.add(entry.getKey());
        }

        return fin;
    }


    @Override
    protected void onPreExecute()
    {
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(context.getResources().getString( R.string.sorting));
        dialog.show();
    }


    @Override
    protected Integer doInBackground(Void... voids) {


        BaseDB db = Room.databaseBuilder(context, BaseDB.class, "populus-database").build();
        BasePhraseDao phraseDao = db.getPhraseDao();
        int zerroDifficultyPhrases = phraseDao.countZerroDifficultyPhrases(NamesSR.getLerningLanguage(context));

        if(zerroDifficultyPhrases > loadingDifficultyTrashHold){
            List<BasePhrase> basePhrases = phraseDao.getAllPhrase(NamesSR.getLerningLanguage(context));
            basePhrases = CALCULATE_PHRASE_DIFICLTY(basePhrases);
            phraseDao.updateEnterys(basePhrases);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Integer result) {
        dialog.dismiss();
    }

}
