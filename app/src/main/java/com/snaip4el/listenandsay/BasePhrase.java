package com.snaip4el.listenandsay;


import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.snaip4el.listenandsay.ConvertersForDB.ListeningResultConverter;
import com.snaip4el.listenandsay.ConvertersForDB.SpeakingResultConverter;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;

@Entity
public class BasePhrase {

    @NonNull
    @PrimaryKey
    public final int id;
    private String language;
    private String phrase;
    private int phraseDifficulty;
    private int charInPhrase;
    private int wordInPhrase;

    @TypeConverters({ListeningResultConverter.class})
    public ListeningResult listeningResult;
    @TypeConverters({SpeakingResultConverter.class})
    public SpeakingResult speakingResult;


    public SpeakingResult getSpeakingResult() {
        return speakingResult;
    }

    public void setSpeakingResult(SpeakingResult speakingResult) {
        this.speakingResult = speakingResult;
    }

    public BasePhrase(int id) {
        this.id = id;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public int getCharInPhrase() {
        return charInPhrase;
    }

    public void setCharInPhrase(int charInPhrase) {
        this.charInPhrase = charInPhrase;
    }

    public int getWordInPhrase() {
        return wordInPhrase;
    }

    public void setWordInPhrase(int wordInPhrase) {
        this.wordInPhrase = wordInPhrase;
    }

    public String getPhrase() {
        return phrase;
    }

    public void setPhrase(String phrase) {
        this.phrase = phrase;
    }

    public int getPhraseDifficulty() {
        return phraseDifficulty;
    }

    public void setPhraseDifficulty(int phraseDifficulty) {
        this.phraseDifficulty = phraseDifficulty;
    }

    @Override
    public String toString() {
        return "BasePhrase{" +
                "id=" + id +
                ", language='" + language + '\'' +
                ", phrase='" + phrase + '\'' +
                ", phraseDifficulty=" + phraseDifficulty +
                ", charInPhrase=" + charInPhrase +
                ", wordInPhrase=" + wordInPhrase +
                ", listeningResult=" + listeningResult +
                ", speakingResult=" + speakingResult +
                '}';
    }
}
