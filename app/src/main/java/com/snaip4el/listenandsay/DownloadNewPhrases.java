package com.snaip4el.listenandsay;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import androidx.room.Room;

import java.util.ArrayList;
import java.util.List;

public class DownloadNewPhrases extends AsyncTask<Void, Void, Integer> {


    private static final String TAG = "DownloadNewPhrases";
    protected Context context;
    protected ProgressDialog dialog;
    private int minUntouchPhrasesInBase;
    private String lang;
    protected int addNewPhraseInBase = 0;

    public DownloadNewPhrases(Context context) {
        this.context = context;
        dialog = new ProgressDialog(context);
        minUntouchPhrasesInBase = NamesSR.getMinPhrasesInBase(context);
        lang = NamesSR.getLerningLanguage(context);

    }

    @Override
    protected void onPreExecute()
    {
        dialog.setCanceledOnTouchOutside(false);
        dialog.setMessage(context.getResources().getString( R.string.dowloadin_page));
        dialog.show();
    }

    @Override
    protected Integer doInBackground(Void... params)
    {
        BaseDB db = Room.databaseBuilder(context, BaseDB.class, "populus-database").build();
        BasePhraseDao phraseDao = db.getPhraseDao();
        int untouchPhrasesInBase = phraseDao.countUnseenPhrases(lang);
        List<BasePhrase> phrases = new ArrayList<>();

        //load if we do not have enough phrases
        if(untouchPhrasesInBase < minUntouchPhrasesInBase){
            int dowload = minUntouchPhrasesInBase - untouchPhrasesInBase;
            int allInBase = phraseDao.countPhrases(lang);
            phrases = TatoebaFetcher.getTailNewPhrases(lang, allInBase, dowload);
            phraseDao.updateEnterys(phrases);
        }

        //if need to load some more
        if(addNewPhraseInBase > 0){
            int allInBase = phraseDao.countPhrases(lang);
            phrases = TatoebaFetcher.getTailNewPhrases(lang, allInBase, addNewPhraseInBase);
            phraseDao.updateEnterys(phrases);
        }

        Log.i(TAG, " downloaded - " + phrases.size());
        return 0;
    }

        @Override
        protected void onPostExecute(Integer result) {
            dialog.dismiss();
        }

}