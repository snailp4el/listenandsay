package com.snaip4el.listenandsay.UI;


import android.content.Context;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;

import com.snaip4el.listenandsay.R;

public class   AlertMessage {

    public AlertMessage(Context context, AlertsTypes alertsTypes) {
        switch (alertsTypes){
            case FIRS:
                showAlertDialog(R.string.attention,R.string.need_inet_attention,context);
                break;
            case BETA_MESSAGE:
                showDialogWithLink(R.string.attention, R.string.beta_message, context);
                break;
        }
    }

    private void showAlertDialog(int Title, int Message, Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        alert.setTitle(Title);
        alert.setMessage(Message);
        alert.setPositiveButton("OK",null);
        alert.show();
    }

    private void showDialogWithLink(int Title, int Message, Context context){
        AlertDialog.Builder alert = new AlertDialog.Builder(context);
        final TextView messageTV = new TextView(context);
        final SpannableString s = new SpannableString(context.getText(Message));
        boolean b =Linkify.addLinks(s, Linkify.ALL);
        messageTV.setText(s);
        messageTV.setMovementMethod(LinkMovementMethod.getInstance());
        alert.setTitle(Title).
                setCancelable(true).
                setPositiveButton("OK",null).
                setView(messageTV).
                show();

    }

    public enum AlertsTypes{
        FIRS,
        BETA_MESSAGE;
    }

}
