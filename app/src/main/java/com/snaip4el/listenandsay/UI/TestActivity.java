package com.snaip4el.listenandsay.UI;

import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.room.Room;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.snaip4el.listenandsay.BaseDB;
import com.snaip4el.listenandsay.BasePhrase;
import com.snaip4el.listenandsay.BasePhraseDao;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;
import java.util.ArrayList;
import java.util.List;

public class TestActivity extends AppCompatActivity {

    private static final String TAG = "TestActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test_activity);

        final TextView textOne = (TextView) findViewById(R.id.test_text_one);
        textOne.setText("one"  );

        TextView textTwo = (TextView) findViewById(R.id.test_text_two);
        textTwo.setText("two" );

        Button button = (Button) findViewById(R.id.test_button);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //ttSplayer.ttsSayText("hello, hello, hello");
            }
        });

        //testing
        //todo for test
        AsyncTask.execute(new Runnable() {
            @Override
            public void run() {
/*
                BaseDB db = Room.databaseBuilder(getApplicationContext(), BaseDB.class, "populus-database").build();
                BasePhraseDao phraseDao = db.getPhraseDao();
                //BasePhrase bp = phraseDao.getBasePhrase(8442747);

                List<BasePhrase> phrases;

                FetchCriteriasListeinig fetchCriteriasListeinig = FetchCriteriasListeinig.getInstance(getApplicationContext());

                List <ListeningResult> results = fetchCriteriasListeinig.getListeningResultsForQuery();


                phrases = phraseDao.getAllPhraseListeningResult("eng",  results);
                //phrases = phraseDao.getAllPhrase();
*/





/*                Log.i(TAG, "test Analize");

                String p = "it was dark by the time I got home\n" +
                        "it was dark by the time I get home\n" +
                        "it was a dark by the time I got home\n" +
                        "it was the dark by the time I got home\n" +
                        "it was dark out by the time I got home";


                ArrayList<String> recognizedPhrases = new ArrayList<>();
                for (String s :  p.split("\n")){
                    recognizedPhrases.add(s);
                }*/


                List <SpeakingResult> results = new ArrayList<>();
                results.add(SpeakingResult.SAID_OK);
                //GETTING PHRASE FROM BASE
                BaseDB db = Room.databaseBuilder(getApplicationContext(), BaseDB.class, "populus-database").build();
                BasePhraseDao phraseDao = db.getPhraseDao();

                List<BasePhrase> basePhrases = phraseDao.getAllPhraseSpeakingResult("eng",results);

                //List<BasePhrase> basePhrases = phraseDao.getAllPhrase();

/*                List<BasePhrase> basePhrases = phraseDao.getAllPhrase();
                basePhrases.get(6).setSpeakingResult(SpeakingResult.SAID_OK);
                basePhrases.get(7).setSpeakingResult(SpeakingResult.SAID_OK);
                basePhrases.get(8).setSpeakingResult(SpeakingResult.SAID_WRONG);
                basePhrases.get(9).setSpeakingResult(SpeakingResult.SAID_WRONG);
                phraseDao.updateEnterys(basePhrases);*/

                Log.e(TAG, "hello");






            }
        });


    }


}
