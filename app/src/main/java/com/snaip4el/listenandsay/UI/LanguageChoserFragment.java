package com.snaip4el.listenandsay.UI;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.TatoebaFetcher;
import com.snaip4el.listenandsay.Utils;

import java.util.ArrayList;



public class LanguageChoserFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public LanguageChoserFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static LanguageChoserFragment newInstance(int columnCount) {
        LanguageChoserFragment fragment = new LanguageChoserFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.language_fragment_list, container, false);

        ArrayList<TatoebaFetcher.Language> languagesStartEmpthy = new ArrayList<>();
        languagesStartEmpthy.add(new TatoebaFetcher.Language(getString(R.string.loading),"","",""));

        final LanguageRecyclerViewAdapter adapter = new LanguageRecyclerViewAdapter(languagesStartEmpthy, mListener);

        new AsyncTask() {
            ArrayList<TatoebaFetcher.Language> languages;
            @Override
            protected Object doInBackground(Object[] objects) {
                languages = TatoebaFetcher.GET_LIST_LANGUAGES_WITH_AUDIO();
                return null;
            }

            @Override
            protected void onPostExecute(Object o) {//есть доступ к UI потоку
                adapter.updateUi(languages);
                super.onPostExecute(o);
            }
        }.execute();



        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(new LinearLayoutManager(context));
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }
            recyclerView.setAdapter(adapter);
        }
        return view;
    }

    @Override
    public void onResume() {
        Utils.CHECK_CONNECTING_MAKE_TOAST(getContext());
        showFirsTimeAttention();
        super.onResume();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void showFirsTimeAttention(){

        if(NamesSR.getLerningLanguage(getContext()) == null){
            new AlertMessage(getContext(),AlertMessage.AlertsTypes.FIRS);
        }

    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        // TODO: Update argument type and name
        void onListFragmentInteraction(TatoebaFetcher.Language item);
    }



}
