package com.snaip4el.listenandsay.UI;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.TatoebaFetcher;

public class LanguageChoser extends AppCompatActivity implements LanguageChoserFragment.OnListFragmentInteractionListener {

    private final String TAG = "LanguageChoser";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.language_choser);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

    }

    @Override
    public void onListFragmentInteraction(TatoebaFetcher.Language item) {
        onSupportNavigateUp();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.listeinig_menu, menu);//here we add particular menu in the activity
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    //This method is called whenever the user chooses to navigate Up within your application's activity hierarchy from the action bar.
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
