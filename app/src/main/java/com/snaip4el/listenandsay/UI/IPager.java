package com.snaip4el.listenandsay.UI;
import java.io.Serializable;

public interface IPager extends Serializable {

    void nextPage();

}
