package com.snaip4el.listenandsay.UI;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.os.Bundle;
import android.util.Log;
import com.snaip4el.listenandsay.Phrase;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.presenters.ListeningPagePresenter;

public class ListeningPagerHost extends PageHost {
    private String TAG = "ListeningPagerHost";

    public ListeningPagerHost() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setContentView(R.layout.listening_pager_host);
        toolbar = findViewById(R.id.toolbar_listening);
        toolbar.setTitle(R.string.listening_task);
        setSupportActionBar(toolbar);
        super.onCreate(savedInstanceState);
    }

    @Override //when activity start to by visible
    protected void onStart() {
        Log.i(TAG, "onStart");
        pager = (ViewPager) findViewById(R.id.listenint_pager_host);
        adapter = new ListeningPageAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        getPhrasesFromBase(); //get phrasses Array.
        super.onStart();
    }



    private class ListeningPageAdapter extends FragmentStatePagerAdapter{

        public ListeningPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                showPreScrean();
            }
            Fragment fragment = new ListeningPage(new ListeningPagePresenter(new Phrase(phrasesArray.get(position))));
            return fragment;
        }

        @Override
        public int getCount() {
            return phrasesArray.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

    }

}
