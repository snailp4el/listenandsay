package com.snaip4el.listenandsay.UI;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.snaip4el.listenandsay.Phrase;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.presenters.SpeakingPagePresenter;

public class SpeakingPageHost extends PageHost {
    private String TAG = "SpeakingPageHost";

    public SpeakingPageHost() {
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate");
        setContentView(R.layout.speaking_pager_host);
        toolbar = findViewById(R.id.toolbar_speaking);
        toolbar.setTitle(R.string.speaking_task);
        setSupportActionBar(toolbar);
        super.onCreate(savedInstanceState);
    }



    @Override //when activity start to by visible
    protected void onStart() {
        Log.i(TAG, "onStart");
        pager = (ViewPager) findViewById(R.id.speaking_pager_host);
        adapter = new SpeakingPageAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        getPhrasesFromBase();//get phrasses Array.
        super.onStart();
    }

    private class SpeakingPageAdapter extends FragmentStatePagerAdapter implements IPager{


        public SpeakingPageAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0){
                showPreScrean();
            }
            Fragment fragment = new SpeakingPage(new SpeakingPagePresenter(new Phrase(phrasesArray.get(position))),this);
            return fragment;
        }

        @Override
        public int getCount() {
            return phrasesArray.size();
        }

        @Override
        public int getItemPosition(@NonNull Object object) {
            return POSITION_NONE;
        }

        @Override
        public void nextPage() {
            pager.arrowScroll(View.FOCUS_RIGHT);
        }
    }

}
