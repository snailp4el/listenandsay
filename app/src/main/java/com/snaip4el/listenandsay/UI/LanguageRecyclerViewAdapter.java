package com.snaip4el.listenandsay.UI;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.TatoebaFetcher;
import com.snaip4el.listenandsay.UI.LanguageChoserFragment.OnListFragmentInteractionListener;

import java.util.ArrayList;

public class LanguageRecyclerViewAdapter extends RecyclerView.Adapter<LanguageRecyclerViewAdapter.ViewHolder> {

    ArrayList<TatoebaFetcher.Language> mValues;
    private final OnListFragmentInteractionListener mListener;
    private final String TAG = "LanguageRecyclerViewAd";
    private int mChekedPosition = -1;
    private TatoebaFetcher.Language selectedLanguage;
    private Context context;

    private String lerningLanguage;
    private String dots = "";

    public void updateUi(ArrayList<TatoebaFetcher.Language> i){
        //set language at last position
        mValues = i;
        int n = 0;

        for (TatoebaFetcher.Language l : mValues){
            if(l.getShortName().equals(lerningLanguage)){
                mChekedPosition = n;
            }
            n++;
        }
        notifyDataSetChanged();

    }


    public LanguageRecyclerViewAdapter(ArrayList<TatoebaFetcher.Language> items, OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;

        final Handler handler = new Handler();

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        lerningLanguage = NamesSR.getLerningLanguage(parent.getContext());
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_fragment_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.mItem = mValues.get(position);
        //holder.mIdView.setText(mValues.get(position).id);
        holder.mLanguageTextView.setText(mValues.get(position).getName());
        holder.mAmountPhrases.setText(mValues.get(position).getPhrasesWithAudio());

        holder.mFalagImageView.setImageBitmap(null);


        if(holder.mItem.getShortName() == ""){
            final Handler handler = new Handler();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    if(holder.mItem.getShortName() == ""){

                        holder.mLanguageTextView.setText(holder.mItem.getName() + dots);
                        dots = dots + ".";
                        if (dots.length() > 3) dots = "";

                        if(holder.mItem.getShortName() == "") handler.postDelayed(this, 200);
                    }

                }
            });
        }

        new AsyncTask() {
            Bitmap bm;
            @Override
            protected Object doInBackground(Object[] objects) {
                if(holder.mItem.getFlagImage()==null){
                    bm =TatoebaFetcher.GETiMGEfLAG(holder.mItem.getImageLink());
                }else {
                    bm = holder.mItem.getFlagImage();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                super.onPostExecute(o);
                holder.mFalagImageView.setImageBitmap(bm);
            }
        }.execute();


        if(position == mChekedPosition){
            holder.mRootLayout.setBackgroundColor(Color.WHITE);
        }else {
            holder.mRootLayout.setBackgroundColor(Color.TRANSPARENT);
        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    Log.i(TAG, "onClick");
                    if (position == mChekedPosition) {//uncheck!!!
/*                        holder.mRootLayout.setBackgroundColor(Color.TRANSPARENT);
                        selectedLanguage = null;
                        mChekedPosition = -1;*/
                    } else {
                        selectedLanguage = holder.mItem;
                        mChekedPosition = position;
                        notifyDataSetChanged();
                    }

                    //save studing language and flag
                    if(selectedLanguage != null){
                        Log.i(TAG, "Checked positon " + mChekedPosition+ " lang " + selectedLanguage.getName() +"shortName "+ selectedLanguage.getShortName());



                        NamesSR.setLerningLanguage(context, selectedLanguage.getShortName());
                        String s = TatoebaFetcher.GETiMGEfLAGsTRING(selectedLanguage.getImageLink());


                        new AsyncTask() {
                            String stringFlag;
                            @Override
                            protected Object doInBackground(Object[] objects) {
                                stringFlag = TatoebaFetcher.GETiMGEfLAGsTRING(selectedLanguage.getImageLink());

                                return null;
                            }

                            @Override
                            protected void onPostExecute(Object o) {
                                super.onPostExecute(o);
                                NamesSR.setLerningLanguageFlag(context, stringFlag);
                                mListener.onListFragmentInteraction(holder.mItem);
                            }
                        }.execute();

                    }

                }
            }
        });


    }





    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final LinearLayout mRootLayout;
        public final ImageView mFalagImageView;
        public final TextView mLanguageTextView;
        public final TextView mAmountPhrases;
        public TatoebaFetcher.Language mItem;


        //constructor
        public ViewHolder(View view) {
            super(view);
            mView = view;
            mFalagImageView = (ImageView) view.findViewById(R.id.languageItemFlag);
            mLanguageTextView = (TextView) view.findViewById(R.id.languageItemName);
            mAmountPhrases = (TextView) view.findViewById(R.id.languageItemAmount);
            mRootLayout = (LinearLayout) view.findViewById(R.id.langItemRoot);


        }

        @Override
        public String toString() {
            return super.toString() + " '" + mLanguageTextView.getText() + "'";
        }
    }
}
