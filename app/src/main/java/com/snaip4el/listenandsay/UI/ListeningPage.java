package com.snaip4el.listenandsay.UI;
import android.content.Context;
import android.os.Bundle;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.databinding.ListeningPageBinding;
import com.snaip4el.listenandsay.presenters.TaskPresenter;

import java.io.File;
public class ListeningPage extends Fragment implements ItaskPage {



    private static String TAG = "ListeningPage";
    ListeningPageBinding binding;
    TaskPresenter taskPresenter;

    public ListeningPage(TaskPresenter taskPresenter) {
        this.taskPresenter = taskPresenter;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.listening_page, container, false);
        taskPresenter.setItaskPage(this);//link betvin TaskPager and ItaskPage
        setRetainInstance (true);
        //todo вылетает если не даны разрешения ankiButton
        if(!NamesSR.isExportToAnki(getContext())){
            binding.toAnki.setVisibility(View.INVISIBLE);
        }
        return binding.getRoot();
    }



    @Override
    public void setUserVisibleHint(boolean visible)
    {
        super.setUserVisibleHint(visible);
        if (visible && isResumed())
        {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override
    public void onResume() {

        super.onResume();
        if (!getUserVisibleHint()) { return; }
        taskPresenter.onPageApperListner();

//        PageHost lph = (ListeningPagerHost) getActivity();
//        lph.showPreScrean();
        //listeners
        binding.play.setOnClickListener(v -> taskPresenter.playButtonIsPressed());
        binding.understand.setOnClickListener(v -> taskPresenter.unerstendButtonPressed());
        binding.notUnderstand.setOnClickListener(v -> taskPresenter.dontUnderstendButtonPressed());
        binding.toAnki.setOnClickListener(v -> taskPresenter.toAnkiButtonIsPressed());

    }

    @Override
    public void onStop() {
        super.onStop();
        taskPresenter.onPageDisapperListner();
    }

    //ItaskPage
    @Override
    public Context getContx() {
        return getContext();
    }

    @Override
    public File[] getExternalFilesDirs() {
        return getActivity().getExternalFilesDirs(null);
    }

    @Override
    public TextView getEnAnsverTV() {
        return binding.enAnswer;
    }

    @Override
    public TextView getTranslationTV() {
        return binding.translation;
    }

    @Override
    public TextView getLinkTV() {
        return binding.link;
    }

    @Override
    public Button getUnderstandButton() {
        return binding.understand;
    }

    @Override
    public Button getDontUnderstandButton() {
        return binding.notUnderstand;
    }

    @Override
    public Button getPlayButton() {
        return binding.play;
    }

    @Override
    public void nextPage() {

    }

}
