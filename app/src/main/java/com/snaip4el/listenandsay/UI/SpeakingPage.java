package com.snaip4el.listenandsay.UI;
import android.Manifest;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import com.ichi2.anki.FlashCardsContract;
import com.snaip4el.listenandsay.Anki.PermissionCheker;
import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.databinding.SpeakingPageBinding;
import com.snaip4el.listenandsay.presenters.SpeakingPagePresenter;
import com.snaip4el.listenandsay.presenters.TaskPresenter;
import java.io.File;

public class SpeakingPage extends Fragment implements SpeakingPagePresenter.IspeakingPageExtra, ItaskPage{

    private static String TAG = "SpeakingPage";
    SpeakingPageBinding binding;
    IPager iPager;
    static final int PERMISSION_ALL = 1;

    static String[] PERMISSIONS = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            FlashCardsContract.READ_WRITE_PERMISSION
    };


    //mvp
    TaskPresenter taskPresenter;

    public SpeakingPage(TaskPresenter taskPresenter, IPager iPager) {
        this.taskPresenter = taskPresenter;
        taskPresenter.setPagExtra(this);
        this.iPager = iPager;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate(inflater, R.layout.speaking_page, container, false);
        taskPresenter.setItaskPage(this);//link betvin TaskPager and ItaskPage
        taskPresenter.setPagExtra(this);
        setRetainInstance(true);
        //ankiButton todo вылетает если не даны разрешения
        if(!NamesSR.isExportToAnki(getContext())){
            binding.toAnki.setVisibility(View.INVISIBLE);
        }
        return binding.getRoot();
    }



    @Override
    public void setUserVisibleHint(boolean visible) {
        super.setUserVisibleHint(visible);
        if (visible && isResumed()) {
            //Only manually call onResume if fragment is already visible
            //Otherwise allow natural fragment lifecycle to call onResume
            onResume();
        }
    }

    @Override //PAGE APPEARS
    public void onResume()
    {
        super.onResume();
        if (!getUserVisibleHint())
        {
            return;
        }
        binding.understand.setOnClickListener(v -> taskPresenter.unerstendButtonPressed());
        binding.notUnderstand.setOnClickListener(v ->  taskPresenter.dontUnderstendButtonPressed());
        taskPresenter.onPageApperListner();

        PermissionCheker.CHECK_ASK_PERMISSION(this);

        binding.play.setOnClickListener(v -> {
            taskPresenter.playButtonIsPressed();
        });
    }


    @Override
    public void onStop() {
        super.onStop();
        taskPresenter.onPageDisapperListner();
    }

    public SpeakingPageBinding getBinding(){
        return binding;
    }


    @Override
    public void runInUiThread(Runnable r) {
        Handler mHandler = new Handler(Looper.getMainLooper());
        mHandler.post(r);
    }

    @Override       //num can be 0.0 - 1.0
    public void SetMicOpacity(float num) {
        if (num < 0.0F || num > 1.0F) num = 0.0F;
        binding.micLogo.setAlpha(num);
    }

    //ItaskPage
    @Override
    public Context getContx() {
        return getContext();
    }

    @Override
    public File[] getExternalFilesDirs() {
        return getActivity().getExternalFilesDirs(null);
    }

    @Override
    public TextView getEnAnsverTV() {
        return binding.enAnswer;
    }

    @Override
    public TextView getTranslationTV() {
        return binding.translation;
    }

    @Override
    public TextView getLinkTV() {
        return binding.link;
    }

    @Override
    public Button getUnderstandButton() {
        return binding.understand;
    }

    @Override
    public Button getDontUnderstandButton() {
        return binding.notUnderstand;
    }

    @Override
    public Button getPlayButton() {
        return binding.play;
    }

    @Override
    public void nextPage() {
        iPager.nextPage();
    }



}
