package com.snaip4el.listenandsay.UI;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import androidx.fragment.app.ListFragment;

import com.snaip4el.listenandsay.DifficultyAnalysis;
import com.snaip4el.listenandsay.DownloadNewPhrases;
import com.snaip4el.listenandsay.FetchCriterias;
import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.Task;
import com.snaip4el.listenandsay.Utils;
import java.util.ArrayList;
import java.util.Locale;

public class TaskFragment extends ListFragment {

    private static final String TAG = "TaskFragment";
    ArrayList<Task> tasks = new ArrayList<>();
    Listner listner;
    private Integer MIN_PHRASES_IN_BASE = 1000;

    interface Listner{
        void itemCliced(long id);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //CREATING THE TASKS name                   image
        tasks.add(new Task(R.string.listening_task, R.drawable.ic_baseline_volume_up_24, new Intent(getActivity(), ListeningPagerHost.class)));
        tasks.add(new Task(R.string.speaking_task, R.drawable.ic_baseline_mic_24, new Intent(getActivity(), SpeakingPageHost.class)));

        CustomAdapter adapter = new CustomAdapter(inflater.getContext());
        setListAdapter(adapter);


        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onResume() {
        Log.i(TAG, "onResume()");
        super.onResume();

        String toDelete = NamesSR.getLerningLanguage(getContext());

        //----check do we have a language
        if(NamesSR.getLerningLanguage(getContext()) != null){
            new DownloadNewPhrases(getContext()).execute();
        }else {
           runLanguageChoser();
        }
        //sort by difficulty if nead
        if(FetchCriterias.getInstance(getContext()).isShowByDifficulty()) new DifficultyAnalysis(getContext()).execute();
        if(NamesSR.getNativeLanguage(getContext()) == null) NamesSR.setNativeLanguage(getContext(), Locale.getDefault().getLanguage());
        Utils.CHECK_CONNECTING_MAKE_TOAST(getContext());
        Log.i(TAG, "end of onResume");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        listner = (Listner) context;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        if (listner != null){
            listner.itemCliced(id);

            startActivity(tasks.get((int) id).intent);
            //chosing the task
        }

    }

    private void runLanguageChoser(){
        Intent intent = new Intent(getActivity(), LanguageChoser.class);
        startActivity(intent);
    }

    private class CustomAdapter  extends ArrayAdapter<Task>{
        public CustomAdapter (Context context) {
            super(context, R.layout.one_task, tasks);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            Task task = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.one_task, null);
            }

            ImageView imageView =((ImageView) convertView.findViewById(R.id.task_image));
            imageView.setBackgroundResource(task.imageResource);
            ((TextView) convertView.findViewById(R.id.task_name)).setText(task.name);

            return convertView;

        }

    }

}


