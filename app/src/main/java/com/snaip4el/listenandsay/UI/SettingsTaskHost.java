package com.snaip4el.listenandsay.UI;
import androidx.appcompat.widget.Toolbar;
import android.os.Bundle;
import android.view.Menu;
import com.snaip4el.listenandsay.R;

public class SettingsTaskHost extends ICommonHost {

    private final String TAG = "SettingsTaskHost";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_task_host);
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.settings);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    //This method is called whenever the user chooses to navigate Up within your application's activity hierarchy from the action bar.
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

}
