package com.snaip4el.listenandsay.UI;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.databinding.ObservableField;
import androidx.fragment.app.Fragment;
import androidx.room.Room;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.Toast;

import com.snaip4el.listenandsay.Anki.PermissionCheker;
import com.snaip4el.listenandsay.BaseDB;
import com.snaip4el.listenandsay.BasePhrase;
import com.snaip4el.listenandsay.BasePhraseDao;
import com.snaip4el.listenandsay.DownloadNewPhrasesReturnInBase;
import com.snaip4el.listenandsay.FetchCriterias;
import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;
import com.snaip4el.listenandsay.TatoebaFetcher;
import com.snaip4el.listenandsay.databinding.SettingsTaskFragmentBinding;
import com.snaip4el.listenandsay.DifficultyAnalysis;

import java.util.List;
import java.util.Locale;
import java.util.TreeSet;

public class SettingsTaskFragment extends Fragment implements DownloadNewPhrasesReturnInBase.stuffListner {

    private static final String TAG = "SettingsTaskFragment";
    private String laguages[];
    private String lerningLanguage;
    TreeSet<String> set = new TreeSet();
    SettingsTaskFragmentBinding binding;
    ObservableField<String> phraseInBase = new ObservableField<>();
    ObservableField<String> willBeShown = new ObservableField<>();
    int downloadAdition = 500;//при нажатии загрузить каждый раз загружаем все больше и больше
    private int lastMesurePhraseInBaseInt = 0;
    private boolean FirstPhrasesInBaseAndSet = true;

    public SettingsTaskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.settings_task_fragment, container, false);
        binding.setPhrases(phraseInBase);//data binding
        binding.setWillBeShown(willBeShown);
        FetchCriterias fetchCriterias = FetchCriterias.getInstance(getContext());
        Log.i(TAG, "fetchCriterias in "+ fetchCriterias);
        binding.setFetchCriterias(fetchCriterias);//data binding
        binding.AddPhraseToAnki.setOnCheckedChangeListener((buttonView, isChecked) -> addToAnkiChecked(isChecked));
        return binding.getRoot();
    }


    @Override
    public void onPause() {

        NamesSR.setNativeLanguage(getContext(),binding.nativeLangSpiner.getSelectedItem().toString());
        NamesSR.setExportToAnki(getContext(),binding.AddPhraseToAnki.isChecked());
        FetchCriterias fetchCriterias = getFetchCreterialsByCheckButtonsPosition();
        if (willBeShown.get().endsWith("0")){
            checkListeningCheckButtons();
            checkSpikingCheckButtons();

            fetchCriterias.setShowUnpresed(true);
            fetchCriterias.setShowUnderstod(true);
            fetchCriterias.setShowDidNotUnderstod(true);

            fetchCriterias.setShowUnspoken(true);
            fetchCriterias.setShowSpokenCorrectlyOnFirsTime(true);
            fetchCriterias.setShowSpokenCorrectly(true);
            fetchCriterias.setShowSpokenWrong(true);
            Toast toast = Toast.makeText(getContext(),
                    R.string.zero_set, Toast.LENGTH_SHORT);
            toast.show();
        }

        fetchCriterias.searilize(getContext());
        super.onPause();
    }



    @Override
    public void onStart() {
        super.onStart();

        binding.AddPhraseToAnki.setChecked(NamesSR.isExportToAnki(getContext()));
        lerningLanguage = NamesSR.getLerningLanguage(getContext());

        //test spiner
        Locale[] l = Locale.getAvailableLocales();

        for(Locale locale: l){
            set.add(locale.getLanguage());
        }

        laguages = new String[set.size()];

        int currentLangInt = 0;
        //String lang = Locale.getDefault().getLanguage();
        String lang = NamesSR.getNativeLanguage(getContext());
        int i = 0;
        for (String s: set){
            laguages[i] = s;
            if (s.equals(lang)){
                currentLangInt = i;
            }
            i++;
        }

        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, laguages); //selected item will look like a spinner set from XML
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        binding.nativeLangSpiner.setAdapter(spinnerArrayAdapter);
        binding.nativeLangSpiner.setSelection(currentLangInt);

        Log.i(TAG, "spinet"+ getActivity());

        //todo test
        String data = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAUCAMAAACtdX32AAACJVBMVEX////4///SHDMAJH3JABDZMUTTHzTKABbMAxzIAAoAE3MAAGEAJ4DrT1vvYGr0/v/ULUIAFXYNNYkAB2vOECdmjcIPOY3RHjQAAFbKAQ0AAGcAAGTQFy/PFiwAAmnVMUbVLkP//v4AH3rn9v1GbK0ADHDOAADk7fbcAAD88PEIMonr+v/87/D/+/v7///mjZbHAAOlyuwAAFHXRlXp8/nXAAD/+fzvs7n/7PAAFnz/k4nLAAYDGnfWUlzPEyoAJX7XP1L//f3bTF3hAAHQLDj59/njSFb9S0H74ujonKLuUl3mWGYWJ37/9vbyY2vaWWT/pp0OIHr///qywNshPZFGgcXt//9Gb64hV6tpn9eiq81VZKP3Ojf1y8kfMIS0vdj+8fUAAFhib6rQMz/0KSScpcnVPktHVJjxv8X/h3u50erNDRbODiYAAm3aNzpWktH0hobi0tr/uLTKARPXO04OQZsQOo1ifrcAIIPP3e0LPJfZUGLj1t1QW5uotNPSPkP3aGRBUZi0p8E/ZKj88vPcU1b/u7gAK4r10NTw+v6rtNLje4TtkJHrgYG8prz66u5AW6Q1cbxghbyostLUISd9v+7C1u4XSKD/n5NVYKCtzexyptrs1djSOkVui8EABXBCXqbUFhthoNlgmNPHq7pTj87nl55AZafqo6r4Ny/tzc9hisAoX7CsxeTqpq3vJy7ZKi/soKni0dmWn8Z4suLptbvOJzA7uKARAAAB3ElEQVQoz12Sd1cTURDFb7JrWd4qLLsxy26yJCEhkSakAGlgUFSUCNIRRIp0e++9N+y994ZK/Xxs8g6HwO+vefeeeeeemYGvG+4O1cUzTFqmYTXHrkpfk8YwvPj+6+T/0Qn8C/8Fdu4gDn7BrnSQ0yX1gb5yYDzkGSiF/6XqEuZtQdx34Ozm3HXAbgTbf/0M/0h8IOcVJWxDpnyspH7Dxi0w9GqIkLaRkMcXh3/Qlb+dY7fmnGw+RFu7SASaKJPmLx+L9ff5nG0cu8ndfzDReuqSVxY1OJ2t35s+tdy7UAbgDMf6cHh/DZB/oulKq9OJJIUGIB6z26s4dn2sEECs2k6dZUlstuzsjAybRZIsemXTS6pjRSp7LRZpkYCVqUhWq7RIwPIkRqPRrGOtqLCazYnSSHUaoGGs4CpQuLYq68jRAh2gOk4dk8n0+8O7FmDo2nXU6ckB3Ky7W/T0wSPdgqL1eLsaURoNXLxMx9KIhjvFT+4/JF5NAaNGpoCyrD272m/QoQrnbqG7z/Pi1XPiABl0A9HA8LO2Hp6uJE+uvQ3UhDvffHahn7a+DirzCz0uiEpHOsrfekLjAKK5wyN/VCHlHHgHqX0MfAt3Ymh2dGY6qPCVS45J7fVjbGAOXcZoRuW5mukAAAAASUVORK5CYII=";
        String saveFlag = NamesSR.getLerningLanguageFlag(getContext());
        if (saveFlag != null){
            data = saveFlag;
        }

        ImageButton imageButton =  getActivity().findViewById(R.id.setingFragmentChoseLangButton);
        imageButton.setImageBitmap(TatoebaFetcher.CONVERTsTRINGtObITMAP(data));

        imageButton.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), LanguageChoser.class);
            startActivity(intent);
        });


        binding.DownloadNewBtn.setOnClickListener(v -> {
            new DownloadNewPhrasesReturnInBase(getContext(), downloadAdition, this).execute();
            downloadAdition= downloadAdition*2;
        });

        //sorting radioButtons
        binding.sortingRadioGroup.setOnCheckedChangeListener((group, checkedId) -> {
            if (checkedId == R.id.SortByDifficulty){
                Log.i(TAG, "onCheckedChanged " + checkedId);
                new DifficultyAnalysis(getContext()).execute();

            }
        });

        //addListners
        View.OnClickListener listeningListner = v -> {
            AsyncTask.execute(() -> phrasesInBaseAndSet());
            checkSpikingCheckButtons();
        };
        binding.ShowUnpresedCheckBox.setOnClickListener(listeningListner);
        binding.ShowUnderstoode.setOnClickListener(listeningListner);
        binding.ShowNotunderstoode.setOnClickListener(listeningListner);

        View.OnClickListener speakingListner = v -> {
            AsyncTask.execute(() -> phrasesInBaseAndSet());
            checkListeningCheckButtons();
        };
        binding.ShowUnspoken.setOnClickListener(speakingListner);
        binding.ShowSpokenCorrectly.setOnClickListener(speakingListner);
        binding.ShowSpokenCorrectlyOnFirstTime.setOnClickListener(speakingListner);
        binding.ShowSpokenWrong.setOnClickListener(speakingListner);

        FirstPhrasesInBaseAndSet = true;
        AsyncTask.execute(() -> phrasesInBaseAndSet() );

    }



    @Override
    public void endLoading() {
        AsyncTask.execute(() -> phrasesInBaseAndSet() );
    }

    private void addToAnkiChecked(Boolean isChecked) {
        if(isChecked){
            //is anki installed
            //ask permission
            Log.i(TAG, "addToAnkiChecked result ");
            PermissionCheker.CHECK_ASK_PERMISSION(this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i(TAG, "onRequestPermissionsResult");

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (!PermissionCheker.CHECK_ANKI(this)){
            binding.AddPhraseToAnki.setChecked(false);
            return;
        }
        binding.AddPhraseToAnki.setChecked(PermissionCheker.CHECK(this));
    }

    //показать количество скаченых фраз, и сколько будет фраз в выборку
    private void phrasesInBaseAndSet(){
        BaseDB db = Room.databaseBuilder(getContext(), BaseDB.class, "populus-database").build();
        BasePhraseDao phraseDao = db.getPhraseDao();
        //base
        int phrasesInt = phraseDao.countPhrases(NamesSR.getLerningLanguage(getContext()));
        if (lastMesurePhraseInBaseInt == phrasesInt) downloadAdition = phrasesInt+ 200;//if amount of phrases don't grow
        phraseInBase.set("" + phrasesInt);
        //set
        FetchCriterias fetchCriterias = getFetchCreterialsByCheckButtonsPosition();
        if (FirstPhrasesInBaseAndSet){
            fetchCriterias = FetchCriterias.getInstance(getContext());
            FirstPhrasesInBaseAndSet = false;
        }

        List<SpeakingResult> speakingResults = fetchCriterias.getSpeakingResultsForQuery();
        List <ListeningResult> listeningResults = fetchCriterias.getListeningResultsForQuery();

        int phraseInSet = phraseDao.getAllPhraseListeningAndSpeakingResult(NamesSR.getLerningLanguage(getContext()), listeningResults ,speakingResults).size();
        willBeShown.set(""+phraseInSet);
    }

    FetchCriterias getFetchCreterialsByCheckButtonsPosition(){
        FetchCriterias fetchCriterias = FetchCriterias.getInstance(getContext());
        //sorted Radio buttons
        fetchCriterias.setShowSimpleFirst(binding.SortByLenght.isChecked());
        fetchCriterias.setShowByDifficulty(binding.SortByDifficulty.isChecked());
        fetchCriterias.setSortByNothing(binding.SortByNothing.isChecked());
        //visibility
        fetchCriterias.setShowUnpresed(binding.ShowUnpresedCheckBox.isChecked());
        fetchCriterias.setShowUnderstod(binding.ShowUnderstoode.isChecked());
        fetchCriterias.setShowDidNotUnderstod(binding.ShowNotunderstoode.isChecked());
        //speaking
        fetchCriterias.setShowUnspoken(binding.ShowUnspoken.isChecked());
        fetchCriterias.setShowSpokenCorrectly(binding.ShowSpokenCorrectly.isChecked());
        fetchCriterias.setShowSpokenCorrectlyOnFirsTime(binding.ShowSpokenCorrectlyOnFirstTime.isChecked());
        fetchCriterias.setShowSpokenWrong(binding.ShowSpokenWrong.isChecked());

        //toDo if no one chosen
        return fetchCriterias;
    }

    void checkSpikingCheckButtons(){
        binding.ShowUnspoken.setChecked(true);
        binding.ShowSpokenCorrectly.setChecked(true);
        binding.ShowSpokenCorrectlyOnFirstTime.setChecked(true);
        binding.ShowSpokenWrong.setChecked(true);
    }

    void checkListeningCheckButtons(){
        binding.ShowUnpresedCheckBox.setChecked(true);
        binding.ShowUnderstoode.setChecked(true);
        binding.ShowNotunderstoode.setChecked(true);
    }



}
