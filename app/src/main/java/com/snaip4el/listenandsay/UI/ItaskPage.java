package com.snaip4el.listenandsay.UI;

import android.content.Context;
import android.widget.Button;
import android.widget.TextView;

import com.snaip4el.listenandsay.SpeechRecognizerSingleton;

import java.io.File;

public interface ItaskPage {

    Context getContx();
    File[] getExternalFilesDirs();

    TextView getEnAnsverTV();
    TextView getTranslationTV();
    TextView getLinkTV();

    Button getUnderstandButton();
    Button getDontUnderstandButton();
    Button getPlayButton();

    void nextPage();

}
