package com.snaip4el.listenandsay.UI;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.LayoutRes;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.snaip4el.listenandsay.R;

abstract class ICommonHost extends AppCompatActivity {

    private int hostId;

    public Intent getIntent(AppCompatActivity appCompatActivity){
        return new Intent(this, this.getClass());
    }



}
