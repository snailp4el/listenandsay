package com.snaip4el.listenandsay.UI;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ScaleDrawable;
import android.media.Image;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.R;
import com.snaip4el.listenandsay.TatoebaFetcher;
import com.snaip4el.listenandsay.Utils;


//task chose activity (start window)
public class TaskHost extends AppCompatActivity implements TaskFragment.Listner {

    private static final String TAG = "TaskHost";
    Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.task_host);
        toolbar = findViewById(R.id.toolbar);

        toolbar.setTitle(" " + getResources().getString(R.string.app_name));

        setSupportActionBar(toolbar);
        //TODO test get page
    }

    @Override
    protected void onPostResume() {
        setFlag();
        super.onPostResume();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.task_menu, menu);//here we add particular menu in the activity
        return super.onCreateOptionsMenu(menu);
    }

    @Override //menu is presed
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_create_order:


                Intent intent = new Intent(this, SettingsTaskHost.class);
                startActivity(intent);
                Log.i(TAG,"setting");
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void itemCliced(long id) {
        Log.i(TAG, "itemCliced "+ id);
    }

    public void setFlag(){
        Drawable drawable = NamesSR.GET_FLAG(this);
        if(drawable != null){
            drawable = Utils.RESIZE(drawable, this);
            toolbar.setLogo(drawable);
        }
    }

}
