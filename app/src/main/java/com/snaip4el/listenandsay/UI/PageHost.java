package com.snaip4el.listenandsay.UI;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.os.AsyncTask;;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.room.Room;
import androidx.viewpager.widget.ViewPager;
import com.snaip4el.listenandsay.BaseDB;
import com.snaip4el.listenandsay.BasePhrase;
import com.snaip4el.listenandsay.BasePhraseDao;
import com.snaip4el.listenandsay.DifficultyAnalysis;
import com.snaip4el.listenandsay.FetchCriterias;
import com.snaip4el.listenandsay.NamesSR;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;
import com.snaip4el.listenandsay.Utils;
import java.util.ArrayList;
import java.util.List;

public abstract class PageHost extends AppCompatActivity {

    private String TAG = "PageHost";

    protected List<BasePhrase> phrasesArray = new ArrayList<>();
    protected float drag;
    protected ViewPager pager;
    FragmentStatePagerAdapter adapter;
    Toolbar toolbar;
    protected int positionComeFromBundle = 0;
    protected static String KEY_BUNDLE_POSITION_INT = "keyForBundlePostionIng";

    @Override
    //This method is called whenever the user chooses to navigate Up within your application's activity hierarchy from the action bar.
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        if (savedInstanceState != null){
            positionComeFromBundle = savedInstanceState.getInt(KEY_BUNDLE_POSITION_INT, 0);
        }
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //getMenuInflater().inflate(R.menu.listeinig_menu, menu);//here we add particular menu in the activity
        Utils.CHECK_CONNECTING_MAKE_TOAST(getApplicationContext());
        assert getSupportActionBar() != null;   //null check
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(KEY_BUNDLE_POSITION_INT, pager.getCurrentItem());
    }

    protected void getPhrasesFromBase(){
        new AsyncTask() {
            List<BasePhrase> pa = new ArrayList<>();
            @Override
            protected Object doInBackground(Object[] objects) {

                //GETTING PHRASE FROM BASE
                BaseDB db = Room.databaseBuilder(getApplicationContext(), BaseDB.class, "populus-database").build();
                BasePhraseDao phraseDao = db.getPhraseDao();

                FetchCriterias fetchCriterias = FetchCriterias.getInstance(getApplicationContext());
                List <SpeakingResult> speakingResults = fetchCriterias.getSpeakingResultsForQuery();
                List <ListeningResult> listeningResults = fetchCriterias.getListeningResultsForQuery();

                if (fetchCriterias.isShowSimpleFirst()){
                    Log.i(TAG,"PageHost fetchCriterias.isShowSimpleFirst()");
                    pa = phraseDao.getAllPhraseListeningAndSpeakingResultShortFirst(NamesSR.getLerningLanguage(getApplicationContext()), listeningResults ,speakingResults);
                }else if(fetchCriterias.isShowByDifficulty()){
                    Log.i(TAG,"PageHost fetchCriterias.isShowByDifficulty()");
                    pa = phraseDao.getAllPhraseListeningAndSpeakingResultSimpeFirst(NamesSR.getLerningLanguage(getApplicationContext()), listeningResults ,speakingResults);
                }else {
                    Log.i(TAG,"PageHost phraseDao.getAllPhraseListeningAndSpeakingResult");
                    pa = phraseDao.getAllPhraseListeningAndSpeakingResult(NamesSR.getLerningLanguage(getApplicationContext()), listeningResults ,speakingResults);
                }


                return null;
            }

            @Override
            protected void onPostExecute(Object o) {
                phrasesArray = pa;


                StringBuilder sb = new StringBuilder("");
                for (BasePhrase basePhrase: phrasesArray){
                    sb.append(basePhrase.id +"\n");
                }

                adapter.notifyDataSetChanged();
                pager.setCurrentItem(positionComeFromBundle);
                super.onPostExecute(o);
            }
        }.execute();
    }

    //help to show user next screan posobility
    public void showPreScrean(){

        if(phrasesArray.size() <= 1){
            return;
        }

        pager.postDelayed(new Runnable() {
            @Override
            public void run() {
                pager.beginFakeDrag();
                drag = 0;
                //density - плотность
                float density = getResources().getDisplayMetrics().density;

                int width = getResources().getDisplayMetrics().widthPixels;

                // 120 is the number of dps you want the page to move in total
                ValueAnimator animator = ValueAnimator.ofFloat(0, width, 0 * density);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator valueAnimator) {
                        float progress = (float) valueAnimator.getAnimatedValue();
                        pager.fakeDragBy(drag - progress);
                        drag = progress;
                    }
                });
                animator.addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animator) { }

                    @Override
                    public void onAnimationEnd(Animator animator) {
                        pager.endFakeDrag();
                    }

                    @Override
                    public void onAnimationCancel(Animator animator) { }

                    @Override
                    public void onAnimationRepeat(Animator animator) { }
                });
                animator.setDuration(2000);
                animator.start();
            }
        }, 500);
    }

}
