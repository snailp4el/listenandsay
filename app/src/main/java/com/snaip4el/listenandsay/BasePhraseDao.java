package com.snaip4el.listenandsay;
import androidx.annotation.Nullable;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.TypeConverters;
import com.snaip4el.listenandsay.ConvertersForDB.ListeningResultConverter;
import com.snaip4el.listenandsay.ConvertersForDB.SpeakingResultConverter;
import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;
import java.util.List;

@Dao
public interface BasePhraseDao {


    // Добавление phrases в бд если есть то заменяем
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void updateEnterys (List<BasePhrase> phrases);

    // Добавление phrases в бд если есть то заменяем
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void updatePhrase (BasePhrase phrase);


    //insert Array
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(List<BasePhrase> phrases);

    //todo get phrases lang criterials



    @Query("SELECT COUNT(language) FROM basephrase WHERE language = :lang")
    int countUnseenPhrases(String lang);


    @Query("SELECT COUNT(language) FROM basephrase WHERE language = :lang")
    int countPhrases(String lang);

    // Удаление Person из бд
    @Delete
    void delete(BasePhrase phrase);


    // Получение всех phrases из бд
    @Nullable
    @Query("SELECT * FROM basephrase")
    List<BasePhrase> getAllPhrase();

    @Nullable
    @Query("SELECT * FROM basephrase WHERE id = :phraseId")
    BasePhrase getBasePhrase(int phraseId);


    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang")
    List<BasePhrase> getAllPhrase(String lang);

    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang AND listeningResult IN (:lr)")
    List<BasePhrase> getAllPhraseListeningResult(String lang, @TypeConverters(ListeningResultConverter.class) List<ListeningResult> lr);

    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang AND speakingResult IN (:results)")
    List<BasePhrase> getAllPhraseSpeakingResult(String lang, @TypeConverters(SpeakingResultConverter.class) List<SpeakingResult> results);

    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang AND speakingResult IN (:results) ORDER BY wordInPhrase")
    List<BasePhrase> getAllPhraseSpeakingResultSimpeFirst(String lang, @TypeConverters(SpeakingResultConverter.class) List<SpeakingResult> results);


    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang AND listeningResult IN (:lr) AND speakingResult IN (:sr)")
    List<BasePhrase> getAllPhraseListeningAndSpeakingResult(String lang,
                                                            @TypeConverters(ListeningResultConverter.class) List<ListeningResult> lr,
                                                            @TypeConverters(SpeakingResultConverter.class) List<SpeakingResult> sr);


    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang AND listeningResult IN (:lr) AND speakingResult IN (:sr) AND phraseDifficulty > 1 ORDER BY phraseDifficulty")
    List<BasePhrase> getAllPhraseListeningAndSpeakingResultSimpeFirst(String lang,
                                                            @TypeConverters(ListeningResultConverter.class) List<ListeningResult> lr,
                                                            @TypeConverters(SpeakingResultConverter.class) List<SpeakingResult> sr);

    @Nullable
    @Query("SELECT * FROM basephrase WHERE language = :lang AND listeningResult IN (:lr) AND speakingResult IN (:sr) ORDER BY wordInPhrase")
    List<BasePhrase> getAllPhraseListeningAndSpeakingResultShortFirst(String lang,
                                                                      @TypeConverters(ListeningResultConverter.class) List<ListeningResult> lr,
                                                                      @TypeConverters(SpeakingResultConverter.class) List<SpeakingResult> sr);

    @Nullable
    @Query("SELECT COUNT(phraseDifficulty) FROM basephrase WHERE language = :lang AND phraseDifficulty = 0")
    int countZerroDifficultyPhrases(String lang);


}
