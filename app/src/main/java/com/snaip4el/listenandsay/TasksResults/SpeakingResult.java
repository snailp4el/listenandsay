package com.snaip4el.listenandsay.TasksResults;

public enum  SpeakingResult {

    DID_NOT_SAY(0),
    SAID_OK_FIRST_TRY(10),
    SAID_OK(5),
    SAID_WRONG(1);



    SpeakingResult(int numInBas) {
        this.numInBas = numInBas;
    }

    private int numInBas;
    public int getNumInBas() {
        return numInBas;
    }

}
