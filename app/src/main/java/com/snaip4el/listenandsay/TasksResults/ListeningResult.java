package com.snaip4el.listenandsay.TasksResults;

public enum ListeningResult {

    DID_NOT_PRESS(0),
    PRESSED_YES(1),
    PRESSED_NO(2);

    ListeningResult(int numInBas) {
        this.numInBas = numInBas;
    }

    private int numInBas;
    public int getNumInBas() {
        return numInBas;
    }

}
