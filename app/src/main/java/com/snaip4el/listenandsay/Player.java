package com.snaip4el.listenandsay;

import android.content.Context;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.util.Log;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class Player{
    private static final String TAG = "Player";
    private static MediaPlayer mediaPlayer = new MediaPlayer();
    private static int playPresed = 0;

    public static void playSound(Phrase phrase, Context context, final IPlayersActions iPlayersActions){

        mediaPlayer.setOnCompletionListener(mp -> iPlayersActions.stopPlayListner(Players.AUDIO));
        if (phrase != null || phrase.getByteSound() != null) {
            try {
                playMp3(phrase.getByteSound());;
            }catch (Exception e){

                if(playPresed >= 2){
                    AlertDialog.Builder builder = new AlertDialog.Builder(context);
                    builder.setTitle(R.string.page_fragment_check_net_title)
                            .setMessage(R.string.page_fragment_check_net_message)
                            .setIcon(R.drawable.settings)
                            .setCancelable(false)
                            .setNegativeButton(R.string.page_fragment_check_net_button,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });
                    AlertDialog alert = builder.create();
                    alert.show();
                }else {
                    iPlayersActions.cantPlay();

                }
            }

        }

    }


    public static void stopPlay(){
        mediaPlayer.stop();
    }




    //TODO: nede to move to other class
    private static void playMp3(byte[] mp3SoundByteArray) {
        try {
            // create temp file that will hold byte array
            File tempMp3 = File.createTempFile("kurchina", "mp3");
            tempMp3.deleteOnExit();
            FileOutputStream fos = new FileOutputStream(tempMp3);
            fos.write(mp3SoundByteArray);
            fos.close();

            // resetting mediaplayer instance to evade problems
            mediaPlayer.reset();

            // In case you run into issues with threading consider new instance like:
            // MediaPlayer mediaPlayer = new MediaPlayer();

            // Tried passing path directly, but kept getting
            // "Prepare failed.: status=0x1"
            // so using file descriptor instead
            FileInputStream fis = new FileInputStream(tempMp3);
            mediaPlayer.setDataSource(fis.getFD());

            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (IOException ex) {
            String s = ex.toString();
            ex.printStackTrace();
        }
    }




}
