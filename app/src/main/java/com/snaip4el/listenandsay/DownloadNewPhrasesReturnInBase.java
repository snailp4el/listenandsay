package com.snaip4el.listenandsay;

import android.content.Context;

import androidx.databinding.ObservableField;
import androidx.room.Room;

public class DownloadNewPhrasesReturnInBase extends DownloadNewPhrases {

    private static final String TAG = "DownloadNewPhrasesReturnInBase";

    DownloadNewPhrasesReturnInBase.stuffListner sl;

    public DownloadNewPhrasesReturnInBase(Context context, int addNew, DownloadNewPhrasesReturnInBase.stuffListner sl) {
        super(context);
        this.addNewPhraseInBase = addNew;
        this.sl = sl;
    }

    @Override
    protected void onPostExecute(Integer result)
    {

        if(result==0)
        {
            //do some thing
        }
        // after completed finished the progressbar
        dialog.dismiss();
        sl.endLoading();
    }


    public interface stuffListner{
        void endLoading();
    }


}
