package com.snaip4el.listenandsay;

/*
if the amount of phrases is insufficient. need to load new phrases from tatoeba
*/

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Log;
import android.widget.Toast;

import androidx.room.Room;

import java.io.IOException;
import java.util.List;
import java.util.Random;

public class Utils {


    private static final String TAG = "Utils";

    private Utils(Context context) {

    }



    //get studieng language +
    //get min phrase amount +
    //get head phrase
    //save firstLast
    //if here is new phrases then update head from tatoeba
    //get amount of phrases
    //update tail from tatoeba.


    //---------------------------for testing---------------------------------//



    public static void showThePhrases(Context context){
        BaseDB db = Room.databaseBuilder(context, BaseDB.class, "populus-database").build();
        BasePhraseDao phraseDao = db.getPhraseDao();
        List<BasePhrase> list;

        list = phraseDao.getAllPhrase();

        for (BasePhrase phrase: list){
            Log.i(TAG, phrase.toString());
        }


        try {
            String first = list.get(0).toString();
            String last = list.get(list.size()-1).toString();
            int size = list.size();
            Log.i(TAG, "---first---- " + first);
            Log.i(TAG, "---last---- " + last);
            Log.i(TAG, "---size---- " + size);

        }catch (Exception e){
            Log.i(TAG, "emthy array");
        }


    }

    public static void CHECK_CONNECTING_MAKE_TOAST(Context context){
        if (!IS_CONNECTED()){
            Toast toast = Toast.makeText(context,
                    R.string.dont_nave_inet, Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public static boolean IS_CONNECTED(){
        final String command = "ping -c 1 tatoeba.com";
        boolean isConnect = true;
        try {
            isConnect = Runtime.getRuntime().exec(command).waitFor() == 0;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return isConnect;
    }


    private static int getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return r.nextInt((max - min) + 1) + min;
    }


    public static Drawable RESIZE(Drawable image , Context context) {
        Bitmap b = ((BitmapDrawable)image).getBitmap();
        Bitmap bitmapResized = Bitmap.createScaledBitmap(b, 100, 70, false);
        return new BitmapDrawable(context.getResources(), bitmapResized);
    }

}
