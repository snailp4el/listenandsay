package com.snaip4el.listenandsay;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class TatoebaFetcher {

    public static final String TAG = "TatoebaFetcher";
    public static final int PHRASESiNpAGE = 100;


    public static @NonNull Integer getAmountOfPages(String lang){
        int amount = 1;
        int phrases = 1;
        String URL = "https://tatoeba.org/eng/audio/index/"+ lang ;


        try{
            Document doc = Jsoup.connect(URL).get();
            Elements elements = doc.select("h2:contains(Sentences in)");
            phrases = stringToInt(elements.first().text());

            //get amount of phrases
            if(phrases%PHRASESiNpAGE != 0){
                amount = phrases/PHRASESiNpAGE+1;
            }else {
                amount = phrases/PHRASESiNpAGE;
            }

            Log.e(TAG, " - " + elements);

        }catch (IOException ex){
            Log.e(TAG, ex.toString());
        }
        Log.e(TAG, "last page is - " + amount);
        return amount;
    }


                                                                     //
    public static List<BasePhrase>getTailNewPhrases(String language, int phrasesInBase, int needToBeLoaded){
        List<BasePhrase> phrases = new ArrayList<>();
        int page = (phrasesInBase/100) + 1;



        while (needToBeLoaded > phrases.size()){
            List<BasePhrase> p = getListBasePhrase(page, language);
            //
            if(p.size() != 0){
                phrases.addAll(p);
            }else {
                break;
            }

            page++;
        }

        return phrases;
    }



    public static Integer stringToInt(String s){
        s = s.replaceAll("[^\\d.]", "");
        int i = Integer.parseInt(s);
        return i;
    }


    //
    public static List<BasePhrase> getListBasePhrase(int pageNumber, String lang){
        String URL = "https://tatoeba.org/eng/audio/index/"+ lang +"?page=" + pageNumber;

        List<BasePhrase> list = new ArrayList();

        Log.i(TAG, "getListBasePhrase - ling - "+ URL);
        try {

            Document doc = Jsoup.connect(URL).get();
            Elements elements = doc.select("[class=\"sentenceContent\"]");

            for(Element element: elements){
                //Log.i(TAG, element.attr("data-sentence-id"));//id
                //Log.i(TAG, element.text());//text
                String s = element.text();
                Log.i(TAG, s.length()+"");
                try {
                    int num = Integer.parseInt(element.attr("data-sentence-id"));
                    String phrase = element.text();
                    BasePhrase p = new BasePhrase(num);
                    int charsInP = phrase.length();
                    int worsdInP = countWordsInPhrase(phrase);
                    Log.i(TAG, phrase + " charsInP "+ charsInP + " worsdInP "+ worsdInP);
                    p.setCharInPhrase(charsInP);
                    p.setWordInPhrase(worsdInP);
                    p.setLanguage(lang);
                    p.setPhrase(phrase);
                    list.add(p);
                }catch (NumberFormatException n){

                }


            }

        } catch (IOException e) {
            e.printStackTrace();
        }


        return list;
    }


    public static int countWordsInPhrase(String s){
        String trim = s.trim();
        if (trim.isEmpty())
            return 0;
        return trim.split("\\s+").length;
    }


    public static String getPhraseTranslationFromTateba(int phraseId, String mLanguageToTranslate){

        String URL = "https://tatoeba.org/eng/sentences/show/"+ phraseId ;
        String translation = "can't get translation";


        try {
            Document doc = Jsoup.connect(URL).get();
            Elements elements = doc.select("div.sentenceContent");

            //elements = elements.select("div[lang="+ FetcherCriterias.getTranslationLanguage() + "]");

            //all language
            //Locale[] ll = Locale.get
            //String lang = Locale.getDefault().getLanguage();
            elements = elements.select("[lang="+ mLanguageToTranslate + "]");
            if(!elements.isEmpty())translation = (elements.first().text());//translatinon


        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Translation - "+ translation);
        return translation;
    }

/*todo
    public static void ADDpHRASEaNDtRANSLATIONiNpHRASE(Phrase phrase){

        String URL = "https://tatoeba.org/eng/sentences/show/"+ phrase.getId() ;
        String translation = "can't get translation";


        try {
            Document doc = Jsoup.connect(URL).get();
            Elements elements = doc.select("div.sentenceContent");

            //elements = elements.select("div[lang="+ FetcherCriterias.getTranslationLanguage() + "]");

            //all language
            //Locale[] ll = Locale.get
            //String lang = Locale.getDefault().getLanguage();
            elements = elements.select("div[lang="+ mLanguageToTranslate + "]");
            if(!elements.isEmpty())translation = (elements.first().text());//translatinon


        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "Translation - "+ translation);
    }
*/

    public static ArrayList<Language> GET_LIST_LANGUAGES_WITH_AUDIO(){
        final ArrayList<Language> list = new ArrayList<>();
        String URL = "https://tatoeba.org/eng/audio/index";

        try {

            Document doc = Jsoup.connect(URL).get();
            Elements elements = doc.select("li");
            elements = elements.select("[class=\"stat\"]");

            for (Element e: elements){
                Log.i(TAG, "\n" + e.toString()+
                "\n " + e.attr("title") +
                "\n imageLink - "  + "https://tatoeba.org"+e.select("img").attr("src") +
                "\n phrasesWithAudio - " + e.select("span").text() +
                "\n shortName - " + e.select("img").attr("alt") +
                "\n name - " + e.select("img").attr("title"));
                String name = (String) e.select("img").attr("title");
                String shortName = e.select("img").attr("alt");
                String imageLink = "https://tatoeba.org"+e.select("img").attr("src");
                String phrasesWithAudio = e.select("span").text();

                list.add(new Language(name, shortName, phrasesWithAudio, imageLink));
            }

            //elements = elements.select("div[lang="+ FetcherCriterias.getTranslationLanguage() + "]");

            //all language
            //Locale[] ll = Locale.get
            //String lang = Locale.getDefault().getLanguage();
/*            elements = elements.select("div[lang="+ mLanguageToTranslate + "]");
            if(!elements.isEmpty())translation = (elements.first().text());//translatinon*/


        } catch (IOException e) {
            e.printStackTrace();
        }

        new AsyncTask() {//докачать изображения вторым потоком чтобы меньше занимать времени
            @Override
            protected Object doInBackground(Object[] objects) {

                for (Language language: list){
                    language.setFlagImage(GETiMGEfLAG(language.getImageLink()));
                }

                return null;
            }
        }.execute();

/*        StringBuilder sb = new StringBuilder();
        for(Language language: list){
            sb.append(language.shortName+ " " + language.getName() +" | ");
        }*/

        return list;
    }


    //class for studing language choser
    public static class Language {
        private String name;
        private String shortName;
        private String phrasesWithAudio;
        private String imageLink;
        private Bitmap flagImage;

        public Language(String name, String shortName, String phrasesWithAudio, String imageLink) {
            this.name = name;
            this.shortName = shortName;
            this.phrasesWithAudio = phrasesWithAudio;
            this.imageLink = imageLink;
        }

        public Bitmap getFlagImage() {
            return flagImage;
        }

        public void setFlagImage(Bitmap flagImage) {
            this.flagImage = flagImage;
        }

        public String getImageLink() {
            return imageLink;
        }

        public String getPhrasesWithAudio() {
            return phrasesWithAudio;
        }

        public String getName() {
            return name;
        }

        public String getShortName() {
            return shortName;
        }

    }

    public static Bitmap GETiMGEfLAG(String URL){

        try {

            Document doc = Jsoup.connect(URL).ignoreContentType(true).get();
            Element element = doc.select("image").first();
            return CONVERTsTRINGtObITMAP(element.attr("xlink:href"));
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }

    public static String GETiMGEfLAGsTRING(String URL){

        try {

            Document doc = Jsoup.connect(URL).ignoreContentType(true).get();
            Element element = doc.select("image").first();
            return element.attr("xlink:href");
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }


    public static Bitmap CONVERTsTRINGtObITMAP(String s){

        String base64Image = s.split(",")[1];
        String imageDataBytes = s.substring(s.indexOf(",")+1);
        InputStream stream = new ByteArrayInputStream(Base64.decode(imageDataBytes.getBytes(), Base64.DEFAULT));
        Bitmap bitmap1 = BitmapFactory.decodeStream(stream);
        return bitmap1;
    }


    public static byte[] getUrlBytes(String urlSpec) throws IOException {
        URL url = new URL(urlSpec);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();


        try {
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            InputStream in = connection.getInputStream();
            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                throw new IOException(connection.getResponseMessage() +
                        ": with " +
                        urlSpec);
            }
            int bytesRead = 0;
            byte[] buffer = new byte[1024];
            while ((bytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, bytesRead);
            }
            out.close();
            return out.toByteArray();
        } finally {
            connection.disconnect();
        }
    }


     static public class AddTextToPhrase extends AsyncTask<Void, Void, Void> {
        private static final String TAG = "DownloadTranslation";
        Phrase phrase;
        Context context;

         public AddTextToPhrase(Phrase phrase, Context context) {
             this.phrase = phrase;
             this.context = context;
         }

         @Override
        protected Void doInBackground(Void... voids) {
             String nativeLanguage = NamesSR.getNativeLanguage(context);
             String URL = "https://tatoeba.org/eng/sentences/show/"+ phrase.getId() ;
             ArrayList<String> phraseTranslations = new ArrayList<>();
             String phraseText = "";
             String googleTranslation = "";

             try {

                 Document doc = Jsoup.connect(URL).ignoreContentType(true).get();
                 //phrase text
                 //phraseText = doc.select("div[class*=\"content\"]").first().text();

                 Elements findPhraseText = doc.select("div[class*=\"content\"]");
                 for (Element element : findPhraseText){
                     if(!element.toString().contains("➜") && !element.toString().contains("#")){
                         //System.out.println(element.select("div").text());
                         phraseText = element.text();
                     }
                 }

                 //translation if have
                 Elements elements = doc.select("div[class=\"translation\"]");
                 for (Element e : elements){
                     if (e.select("img[alt*=\""+nativeLanguage+"\"]").size() != 0){
                        System.out.println(e.select("div[class=\"text\"]").text());
                        phraseTranslations.add(e.select("div[class=\"text\"]").text());
                     }
                 }



                 Log.i(TAG, " text phrase " + phraseTranslations.size());

             } catch (Exception e) {
                 e.printStackTrace();
             }

            //todo translation google
             if(0 == 0 ){
                 String from = Languages.tatoebaToGoole.get(phrase.getLang());

                try {
                    googleTranslation = Translator.callUrlAndParseResult(from, NamesSR.getNativeLanguage(context),phraseText);
                }catch (Exception e){
                    Log.e(TAG, e.getMessage());
                }
             }
            Log.i(TAG, googleTranslation);
            phrase.setPhrase(phraseText);
            phrase.setGoogleTranslation(googleTranslation);
            phrase.setTranslation(phraseTranslations);


            return null;
        }
    }


    public static class DownloadSoundToPhrase extends AsyncTask<Phrase, Void, Void>{
        private static final String TAG = "DownloadSoundToPhrase";


        @Override
        protected Void doInBackground(Phrase... phrases) {
            Log.i(TAG, "doInBackground");

            do{
                try {
                    Log.i(TAG, "setByteSound");
                    phrases[0].setByteSound(TatoebaFetcher.getUrlBytes("https://audio.tatoeba.org/sentences/"+phrases[0].getLang()+"/"+phrases[0].getId()+".mp3"));
                } catch (IOException e) {
                    e.printStackTrace();
                    break;
                }
            }while (phrases[0].getByteSound()== null);

            return null;
        }

    }



}
