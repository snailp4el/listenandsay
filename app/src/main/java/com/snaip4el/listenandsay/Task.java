package com.snaip4el.listenandsay;
import android.content.Intent;


/*
a task body
*/

public class Task {

    final public int name;
    final public int imageResource;
    final public Intent intent;


    public Task(int name, int imageResource, Intent intent) {
        this.name = name;
        this.imageResource = imageResource;
        this.intent = intent;
    }


}
