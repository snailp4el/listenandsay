package com.snaip4el.listenandsay;

import android.content.Context;
import android.widget.ProgressBar;

public interface IRecognizersNeeds {

    public void OnStopRecognition();

    public ProgressBar getProgressBar();

    public Context getContext();

}
