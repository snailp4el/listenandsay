package com.snaip4el.listenandsay;
import android.util.Log;

import androidx.annotation.Nullable;

import com.snaip4el.listenandsay.UI.SpeakingPage;

import java.util.ArrayList;
import java.util.List;

//gets arrays answers
//and checks right or wrong
//if wrong return wrongest word.
public class AnswerTextAnalyzer {

    private static final String TAG = "AnswerTextAnalyzer";


    @Nullable
    public static AnswerAnalisysResult ANALYZE(List<String> resultsArr, Players players, String said){
        AnswerAnalisysResult ar = null;
        if(players == Players.AUDIO){
            ar = ANALYZE_PHRASE(resultsArr, said);
        }
        if (players == Players.TTS){
            ar = ANALYZE_WORD(resultsArr, said);
        }
        return ar;
    }

    private static AnswerAnalisysResult ANALYZE_WORD(List<String> results, String whatWasPlayd){
        boolean answeredCorrectly = false;
        String wrongestWord = "";

        for (String usersWord: results){
            if (areWordsEqals(usersWord, whatWasPlayd)){//write
                answeredCorrectly = true;
                Log.i(TAG, "user sed Write Word nead repete the phrase");
                break;
            }
        }

        if(!answeredCorrectly) wrongestWord = whatWasPlayd;
        return new AnswerAnalisysResult(wrongestWord, answeredCorrectly);
    }


    private static AnswerAnalisysResult ANALYZE_PHRASE(List<String> results, String whatWasPlayd){

        boolean answeredWrite = false;
        String wordForRepite = "";
        ArrayList<OneWordIner> writeAnsverSplitedArray = new ArrayList<>();

        // создаем массив с щетчиком слов на каждое лово фразы правильного ответа
        for (String word: spaceCleaner(whatWasPlayd).split(" ")){
            Log.i(TAG, "creat writ array ++++"+ word);
            writeAnsverSplitedArray.add(new OneWordIner(word));
        }

        //Для каждой фразы из нескольких вариантов распознного ответа
        for (String onePhrase: results){
            int wordStep = 0;
            boolean isThePhraseDisame7 = true;
            String forLog = "";

            //divide phrase in to words
            for(String oneWord : onePhrase.split(" ")){
                OneWordIner oneWordIner;
                try {
                    oneWordIner = writeAnsverSplitedArray.get(wordStep);//
                }catch (Exception e){
                    break;
                }


                //todo если фраза сказана не полностью то все равно распознается как правильная
                //в каждом OneWordIner есть индекс совпадений
                if (areWordsEqals(oneWord, oneWordIner.word)) {
                    oneWordIner.intFoundInUserAnsversPuluseOne();
                }else {
                    isThePhraseDisame7 = false;
                }
                forLog += oneWord + " " + areWordsEqals(oneWord, oneWordIner.word) + " ||| ";
                wordStep++;
            }
            Log.i(TAG, forLog);
            if(isThePhraseDisame7){
                answeredWrite = true;
                Log.i(TAG, "user said Write Phrase nex page");

            }

            Log.i(TAG, "end of compareUserAnsver condition " + whatWasPlayd + " badWord " + wordForRepite );


        }

        //find the wrongest word;
        if(!answeredWrite){
            int minestWord = 100000;
            for(OneWordIner oneWordIner :writeAnsverSplitedArray){
                if(oneWordIner.intFoundInUserAnsvers < minestWord){
                    minestWord = oneWordIner.intFoundInUserAnsvers;
                    wordForRepite = oneWordIner.word;

                }
            }

        }

        Log.i(TAG, "end of compareUserAnsver condition " + whatWasPlayd + " badWord " + wordForRepite );
        AnswerAnalisysResult answerAnalisysResult = new AnswerAnalisysResult( wordForRepite, answeredWrite);

        return answerAnalisysResult;
    }//ANALYZE_PHRASE

    public static class AnswerAnalisysResult{

        public final String wrongestWord;
        public final boolean saidCorrectly;

        public AnswerAnalisysResult (String wrongestWord, boolean saidCorrectly) {
            this.wrongestWord = wrongestWord;
            this.saidCorrectly = saidCorrectly;
        }

        @Override
        public String toString() {
            return "AnswerAnalisysResult{" +
                    ", wrongestWord='" + wrongestWord + '\'' +
                    ", saidCorrectly=" + saidCorrectly +
                    '}';
        }

    }

    private static String spaceCleaner(String s){
        String result = s;
        s = s.replaceAll("     ", " ");
        s = s.replaceAll("    ", " ");
        s = s.replaceAll("   ", " ");
        s = s.replaceAll("  ", " ");

        return s;
    }

    static class OneWordIner {

        String word;
        int intFoundInUserAnsvers = 0;

        void intFoundInUserAnsversPuluseOne(){ intFoundInUserAnsvers++; }

        OneWordIner(String w){
            this.word = w;        }

    }

    private static boolean areWordsEqals(String original, String voice){
        original = textCleaner(original);
        voice = textCleaner(voice);
        if(voice.equals("tom") ||
                voice.equals("mary")){
            return true;
        }
        if(voice.equals(original)){
            return true;
        }

        return false;
    }

    public static String textCleaner(String s){
        if (s == null)return "";
        //s = s.replaceAll("Tom", "");
        s = s.toLowerCase();
        s = s.replaceAll("[^\\p{L}]", "");

        return s;
    }



}
