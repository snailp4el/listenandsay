package com.snaip4el.listenandsay;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognitionListener;
import android.speech.RecognizerIntent;
import android.speech.SpeechRecognizer;
import android.util.Log;
import java.util.List;
import java.util.Locale;

public class SpeechRecognizerSingleton{

    private static final String TAG = "SpeechRecognizerSing";
    private static SpeechRecognizerSingleton speechRecognizerSingleton;
    private static SpeechRecognizer speechRecognizer;
    private static RecognitionListener recognitionListener;
    private static Intent recognizerIntent;
    private static SpeechRecognizerSingleton.IRecognizersNeeds iRecognizersNeeds;
    private static Players lastPlayer;

    private static int hashContex = 0;
    private static float defoultMicOpasity = 0.1F;

    private SpeechRecognizerSingleton() {
    }

    public static SpeechRecognizerSingleton getInstance(SpeechRecognizerSingleton.IRecognizersNeeds iRecNeeds){

        iRecognizersNeeds = iRecNeeds;

        if(speechRecognizerSingleton == null || hashContex != iRecNeeds.getContext().hashCode()) {
            speechRecognizerSingleton = new SpeechRecognizerSingleton();
            speechRecognizer = android.speech.SpeechRecognizer.createSpeechRecognizer(iRecognizersNeeds.getContext());
            createRecognizeListner();
            speechRecognizer.setRecognitionListener(recognitionListener);
            recognizerIntent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            Locale locale = NamesSR.GET_LEARNING_LANGUAGE_LOCALE(iRecognizersNeeds.getContext());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, locale.getLanguage());
            recognizerIntent.putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 6);
        }

        speechRecognizer.stopListening();
        speechRecognizer.cancel();


        hashContex = iRecNeeds.getContext().hashCode();
        return speechRecognizerSingleton;
    }

    public void destroy(){
        speechRecognizer.destroy();
    }

    private static void createRecognizeListner(){

        recognitionListener = new RecognitionListener() {
            private float lastToShowEq = 0;
            Float minEq = 0.3F;
            @Override
            public void onReadyForSpeech(Bundle params) {

            }

            @Override
            public void onBeginningOfSpeech() {
                lastToShowEq = 10;
            }

            @Override
            //эквалайзер
            public void onRmsChanged(float rmsdB) {
                if (rmsdB <= 0) rmsdB = 0.001F;
                if (rmsdB >= 10) rmsdB = 10;
                float percentRmsdB = rmsdB/10*100;
                float toShow = percentRmsdB*0.7F/100+ minEq;
                iRecognizersNeeds.setMicOpasity(toShow);
            }

            @Override
            public void onBufferReceived(byte[] buffer) {

            }

            @Override
            public void onEndOfSpeech() {
                Log.i(TAG, "onEndOfSpeech");
                //say_UI_recognitionStop();
            }

            @Override
            public void onError(int error) {
                iRecognizersNeeds.OnRecognitionError(error);
                iRecognizersNeeds.setMicOpasity(defoultMicOpasity);
                Log.d(TAG, "FAILED " + getErrorText(error));
            }

            @Override
            public void onResults(Bundle results) {
                List<String> resultOfRecognition;
                Log.i(TAG, "onResults");
                resultOfRecognition = results.getStringArrayList(android.speech.SpeechRecognizer.RESULTS_RECOGNITION);
                StringBuilder text = new StringBuilder("\n");
                for (String result : resultOfRecognition)
                    text.append(result + "\n") ;
                Log.i(TAG, text.toString());
                iRecognizersNeeds.setMicOpasity(defoultMicOpasity);
                iRecognizersNeeds.OnStopRecognition(resultOfRecognition, lastPlayer);
            }

            @Override
            public void onPartialResults(Bundle partialResults) {

            }

            @Override
            public void onEvent(int eventType, Bundle params) {

            }
        };
    }

    public void startListening(Players player){
        lastPlayer = player;
        speechRecognizer.startListening(recognizerIntent);
    }



    public void stopListenig(){
        Log.i(TAG, "stopListenig ");
        speechRecognizer.stopListening();
    }







    public static String getErrorText(int errorCode) {
        String message;
        switch (errorCode) {
            case android.speech.SpeechRecognizer.ERROR_AUDIO:
                message = "Audio recording error";
                break;
            case android.speech.SpeechRecognizer.ERROR_CLIENT:
                message = "Client side error";
                break;
            case android.speech.SpeechRecognizer.ERROR_INSUFFICIENT_PERMISSIONS:
                message = "Insufficient permissions";
                break;
            case android.speech.SpeechRecognizer.ERROR_NETWORK:
                message = "Network error";
                break;
            case android.speech.SpeechRecognizer.ERROR_NETWORK_TIMEOUT:
                message = "Network timeout";
                break;
            case android.speech.SpeechRecognizer.ERROR_NO_MATCH:
                message = "No match";
                break;
            case android.speech.SpeechRecognizer.ERROR_RECOGNIZER_BUSY:
                message = "RecognitionService busy";
                break;
            case android.speech.SpeechRecognizer.ERROR_SERVER:
                message = "error from server";
                break;
            case android.speech.SpeechRecognizer.ERROR_SPEECH_TIMEOUT:
                message = "No speech input";
                break;
            default:
                message = "Didn't understand, please try again.";
                break;
        }
        return message;
    }

    public interface IRecognizersNeeds {
        public void setMicOpasity(float num);
        public void OnStopRecognition(List<String> resultOfRecognition, Players playerWhasUsed);
        public void OnRecognitionError(int Nerror);
        public Context getContext();
    }


}
