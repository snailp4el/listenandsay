package com.snaip4el.listenandsay;


import java.util.ArrayList;
import java.util.Arrays;

public class Phrase{

    private BasePhrase basePhrase;
    private byte[] byteSound;
    private String mPhrase ="";
    private ArrayList<String> mTranslation;
    private String googleTranslation = "";
    private int nOfTry = 1;
    private int nWordTry = 0;

    public int getnWordTry() {
        return nWordTry;
    }

    public void setnWordTry(int nWordTry) {
        this.nWordTry = nWordTry;
    }

    public int getnOfTry() {
        return nOfTry;
    }

    public void setnOfTryPulusOne() {
        this.nOfTry++;
    }

    public Phrase(BasePhrase basePhrase) {
        this.basePhrase = basePhrase;
    }

    public String getGoogleTranslation() {
        return googleTranslation;
    }

    public void setGoogleTranslation(String googleTranslation) {
        this.googleTranslation = googleTranslation;
    }

    public String getLang(){
       return basePhrase.getLanguage();
    }

    public String getPhrase() {
        return mPhrase;
    }

    public void setPhrase(String mPhrase) {
        this.mPhrase = mPhrase;
    }

    public ArrayList<String> getTranslation() {
        return mTranslation;
    }

    public void setTranslation(ArrayList<String> mTranslation) {
        this.mTranslation = mTranslation;
    }

    public int getId(){
        return basePhrase.id;
    }

    public byte[] getByteSound() {
        return byteSound;
    }

    public void setByteSound(byte[] byteSound) {
        this.byteSound = byteSound;
    }

    public BasePhrase getBasePhrase() {
        return basePhrase;
    }

    public void setBasePhrase(BasePhrase basePhrase) {
        this.basePhrase = basePhrase;
    }


    @Override
    public String toString() {
        return "Phrase{" +
                "basePhrase=" + basePhrase +
                ", byteSound=" + Arrays.toString(byteSound) +
                ", mPhrase='" + mPhrase + '\'' +
                ", mTranslation=" + mTranslation +
                ", googleTranslation='" + googleTranslation + '\'' +
                '}';
    }
}

