package com.snaip4el.listenandsay;

import android.content.Context;
import android.util.Log;

import com.snaip4el.listenandsay.TasksResults.ListeningResult;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class FetchCriterias implements Serializable {

    private static final String SERIALISE_FILE = "fetchCridentialSpeaking";
    private static final String TAG = "FetchCriteriasSpeaking";

    boolean showSimpleFirst = false;    //show short firs
    boolean showByDifficulty = false;
    boolean sortByNothing = true;       //show phrase with often use words first

    boolean showUnspoken = true;
    boolean showSpokenCorrectly = false;
    boolean showSpokenCorrectlyOnFirsTime = false;
    boolean showSpokenWrong = true;

    boolean showUnpresed = true;
    boolean showUnderstod = false;
    boolean showDidNotUnderstod = true;

    private FetchCriterias() { }


    public boolean isSortByNothing() {
        return sortByNothing;
    }

    public void setSortByNothing(boolean sortByNothing) {
        this.sortByNothing = sortByNothing;
    }

    public boolean isShowByDifficulty() {
        return showByDifficulty;
    }

    public void setShowByDifficulty(boolean showByDifficulty) {
        this.showByDifficulty = showByDifficulty;
    }

    public boolean isShowUnspoken() {
        return showUnspoken;
    }

    public void setShowUnspoken(boolean showUnspoken) {
        this.showUnspoken = showUnspoken;
    }

    public boolean isShowUnpresed() {
        return showUnpresed;
    }

    public void setShowUnpresed(boolean showUnpresed) {
        this.showUnpresed = showUnpresed;
    }

    public boolean isShowSpokenCorrectlyOnFirsTime() {
        return showSpokenCorrectlyOnFirsTime;
    }

    public void setShowSpokenCorrectlyOnFirsTime(boolean showSpokenCorrectlyOnFirsTime) {
        this.showSpokenCorrectlyOnFirsTime = showSpokenCorrectlyOnFirsTime;
    }

    public boolean isShowSpokenCorrectly() {
        return showSpokenCorrectly;
    }

    public void setShowSpokenCorrectly(boolean showSpokenCorrectly) {
        this.showSpokenCorrectly = showSpokenCorrectly;
    }

    public boolean isShowSpokenWrong() {
        return showSpokenWrong;
    }

    public void setShowSpokenWrong(boolean showSpokenWrong) {
        this.showSpokenWrong = showSpokenWrong;
    }

    public boolean isShowSimpleFirst() {
        return showSimpleFirst;
    }

    public void setShowSimpleFirst(boolean showSimpleFirst) {
        this.showSimpleFirst = showSimpleFirst;
    }


    //////////////////listening


    public boolean isShowUnderstod() {
        return showUnderstod;
    }

    public void setShowUnderstod(boolean showUnderstod) {
        this.showUnderstod = showUnderstod;
    }

    public boolean isShowDidNotUnderstod() {
        return showDidNotUnderstod;
    }

    public void setShowDidNotUnderstod(boolean showDidNotUnderstod) {
        this.showDidNotUnderstod = showDidNotUnderstod;
    }





    public static FetchCriterias getInstance(Context context){
        Log.i(TAG, "DISERIALIZE");


        FetchCriterias fc = new FetchCriterias();

        try {
            String filePath = context.getFilesDir().getPath().toString() + "/"+SERIALISE_FILE;

            FileInputStream fileIn = new FileInputStream(filePath);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            fc = (FetchCriterias) in.readObject();
            in.close();
            fileIn.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return fc;
    }

    public void searilize( Context context){

        try {

            String filePath = context.getFilesDir().getPath().toString() + "/"+SERIALISE_FILE;

            //File file = new File(filePath);
            FileOutputStream fileOut = new FileOutputStream(filePath);
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(this);
            out.close();
            fileOut.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Log.i(TAG, "SERIALIZE");
    }

    //for room querys
    public List<SpeakingResult> getSpeakingResultsForQuery(){
        List<SpeakingResult> lr = new ArrayList<>();
        if(showSpokenCorrectly) lr.add(SpeakingResult.SAID_OK);
        if(showSpokenCorrectlyOnFirsTime) lr.add(SpeakingResult.SAID_OK_FIRST_TRY);
        if(showSpokenWrong) lr.add(SpeakingResult.SAID_WRONG);
        if(showUnspoken) lr.add(SpeakingResult.DID_NOT_SAY);
        return lr;
    }

    //for room querys
    public List<ListeningResult> getListeningResultsForQuery(){
        List<ListeningResult> lr = new ArrayList<>();
        if(showUnderstod) lr.add(ListeningResult.PRESSED_YES);
        if(showDidNotUnderstod) lr.add(ListeningResult.PRESSED_NO);
        if(showUnpresed) lr.add(ListeningResult.DID_NOT_PRESS);
        return lr;
    }

    @Override
    public String toString() {
        return "FetchCriterias{" +
                "showSimpleFirst=" + showSimpleFirst +
                ", showByDifficulty=" + showByDifficulty +
                ", showUnspoken=" + showUnspoken +
                ", showSpokenCorrectly=" + showSpokenCorrectly +
                ", showSpokenCorrectlyOnFirsTime=" + showSpokenCorrectlyOnFirsTime +
                ", showSpokenWrong=" + showSpokenWrong +
                ", showUnpresed=" + showUnpresed +
                ", showUnderstod=" + showUnderstod +
                ", showDidNotUnderstod=" + showDidNotUnderstod +
                '}';
    }
}
