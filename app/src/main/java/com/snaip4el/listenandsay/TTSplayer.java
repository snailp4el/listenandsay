package com.snaip4el.listenandsay;

import android.content.Context;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.util.Log;

import java.util.HashMap;
import java.util.Locale;

public class TTSplayer{
    private static TTSplayer ttSplayer;
    public static TextToSpeech tts;
    private static final String TAG = "TTSplayer";
    private static Locale sLocale;
    private static int hashContex = 0;

    public static TTSplayer getInstance(Locale locale, IPlayersActions iPlayersActions, Context context){

        sLocale = locale;

        if (ttSplayer == null || hashContex != context.hashCode()){
            ttSplayer = new TTSplayer();

            tts=new TextToSpeech(context, status -> {

                if(status == TextToSpeech.SUCCESS){
                    int result=tts.setLanguage(sLocale);

                    tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                        @Override
                        public void onStart(String utteranceId) {

                        }

                        @Override
                        public void onDone(String utteranceId) {
                            iPlayersActions.stopPlayListner(Players.TTS);
                        }

                        @Override
                        public void onError(String utteranceId) {
                            iPlayersActions.cantPlay();
                        }
                    });
                    tts.setPitch(0.9f);
                    tts.setSpeechRate(0.5f);
                    if(result==TextToSpeech.LANG_MISSING_DATA ||
                            result==TextToSpeech.LANG_NOT_SUPPORTED){
                        Log.e(TAG, "This Language is not supported");
                    }
                    else{
                    }
                }
                else
                    Log.e(TAG, "TTS Initilization Failed!");
            });
        }else {
            tts.stop();
        }


        hashContex = context.hashCode();
        return ttSplayer;
    }

    public void ttsSayText(String text){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            String utteranceId = ttSplayer.hashCode() + "";
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, utteranceId);
        } else {
            HashMap<String, String> map = new HashMap<>();
            map.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "MessageId");
            tts.speak(text, TextToSpeech.QUEUE_FLUSH, map);
        }
    }

    public void ttsStop(){
        tts.stop();
    }

}
