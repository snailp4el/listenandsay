package com.snaip4el.listenandsay.Anki;

import android.content.Context;
import android.widget.Toast;
import com.snaip4el.listenandsay.Phrase;
import com.snaip4el.listenandsay.R;
import java.io.File;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Set;

public class AnkiSender {

    private  static AnkiSender ankiSender;
    private static AnkiDroidHelper mAnkiDroid;
    private static String phrasePath;
    private static final int AD_PERM_REQUEST = 0;
    private static Context cont;


    private AnkiSender() {
    }

    public static  AnkiSender getInstens(Context context){

        if (ankiSender == null) {
            ankiSender = new AnkiSender();
            mAnkiDroid = new AnkiDroidHelper(context);
            cont = context;
        }
        return ankiSender;
    }

    public AnkiDroidHelper getmAnkiDroid() {
        return mAnkiDroid;
    }

    public void addCardsToAnkiDroid(final Phrase data, Context context) {

        Long deckId = getDeckId();
        Long modelId = getModelId();
        if ((deckId == null) || (modelId == null)) {
            // we had an API error, report failure and return
            Toast.makeText(context, context.getString(R.string.card_add_fail), Toast.LENGTH_LONG).show();
            return;
        }
        String[] fieldNames = mAnkiDroid.getApi().getFieldList(modelId);
        if (fieldNames == null) {
            // we had an API error, report failure and return
            Toast.makeText(context, context.getString(R.string.card_add_fail), Toast.LENGTH_LONG).show();
            return;
        }
        // Build list of fields and tags
        LinkedList<String []> fields = new LinkedList<>();
        LinkedList<Set<String>> tags = new LinkedList<>();

        //"Sound","Phrase","Translation"
        String[] flds = {"[sound:"+data.getId()+".mp3]",data.getPhrase(),""+ data.getTranslation()};



        tags.add(AnkiDroidConfig.TAGS);
        fields.add(flds);


        //save the sound file




        // Remove any duplicates from the LinkedLists and then add over the API
        mAnkiDroid.removeDuplicates(fields, tags, modelId);
        int added = mAnkiDroid.getApi().addNotes(modelId, deckId, fields, tags);
        if (added != 0) {
            Toast.makeText(context, context.getString(R.string.n_items_added, added), Toast.LENGTH_LONG).show();
        } else {
            // API indicates that a 0 return value is an error
            Toast.makeText(context, context.getString(R.string.card_add_fail), Toast.LENGTH_LONG).show();
        }


    }



    /**
     * get the deck id
     * @return might be null if there was a problem
     */
    private Long getDeckId() {
        Long did = mAnkiDroid.findDeckIdByName(AnkiDroidConfig.getDeskName(cont));
        if (did == null) {
            did = mAnkiDroid.getApi().addNewDeck(AnkiDroidConfig.getDeskName(cont));
            mAnkiDroid.storeDeckReference(AnkiDroidConfig.getDeskName(cont), did);
        }
        return did;
    }

    /**
     * get model id
     * @return might be null if there was an error
     */
    private Long getModelId() {
        Long mid = mAnkiDroid.findModelIdByName(AnkiDroidConfig.MODEL_NAME, AnkiDroidConfig.FIELDS.length);
        //если нету нашего типа записи то добовляем.
        if (mid == null) {
            mid = mAnkiDroid.getApi().addNewCustomModel(AnkiDroidConfig.MODEL_NAME, AnkiDroidConfig.FIELDS,
                    AnkiDroidConfig.CARD_NAMES, AnkiDroidConfig.QFMT, AnkiDroidConfig.AFMT, AnkiDroidConfig.CSS, getDeckId(), null);
            mAnkiDroid.storeModelReference(AnkiDroidConfig.MODEL_NAME, mid);
        }
        return mid;
    }

    public static void getLastChangedAnkiFolder(){

    }

    public static String getPhrasePath(File[] files) {

        //get path to anki files if we do not have
        if (phrasePath == null){
            ArrayList<String> list = FileWorker.GET_EXTERNAL_FOLDERS_OF_PROGRAMM("AnkiDroid", files);
            File lastModificed = FileWorker.LAST_CHANGED_FOLDER(list);
            File[] filesInAnki = lastModificed.listFiles();
            String phrasePath = lastModificed.getPath();
            AnkiSender.setPhrasePath(phrasePath);
        }

        return phrasePath;
    }

    public static void setPhrasePath(String phrasePath) {
        AnkiSender.phrasePath = phrasePath;
    }
}
