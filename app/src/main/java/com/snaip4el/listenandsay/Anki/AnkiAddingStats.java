package com.snaip4el.listenandsay.Anki;

public class AnkiAddingStats {
    private boolean canAddToAnki;
    private String massageToUser;

    public AnkiAddingStats(boolean canAddToAnki, String massageToUser) {
        this.canAddToAnki = canAddToAnki;
        this.massageToUser = massageToUser;
    }

    public boolean isCanAddToAnki() {
        return canAddToAnki;
    }

    public String getMassageToUser() {
        return massageToUser;
    }
}
