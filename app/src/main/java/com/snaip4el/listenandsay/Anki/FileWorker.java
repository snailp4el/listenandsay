package com.snaip4el.listenandsay.Anki;

import android.app.Activity;
import android.util.Log;

import com.snaip4el.listenandsay.Phrase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class FileWorker {

    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0 ;
    private static final String TAG = "FileWorker";


    public static ArrayList GET_EXTERNAL_FOLDERS_OF_PROGRAMM(String folderName, File[] fs){

        ArrayList<String> arrayList = new ArrayList<>();
        //get all storages in phone
        File externalStorages[] = fs;

        try {

            for (File es: externalStorages){
                String exF = es.toString();
                exF = exF.split("/Android/data")[0];

                File file = new File(exF);
                File[] files = file.listFiles();

                for (File f: files){
                    Log.i(TAG, "getAnkiExternalFolder()" + f.toString());
                    if(f.toString().contains(folderName)){
                        arrayList.add(f.toString());
                    }
                }

            }

        }catch (Exception e){

        }
        return arrayList;
    }


    //возврящает путь к колекции в папке ANKI
    public static File LAST_CHANGED_FOLDER(ArrayList<String> folders){
        long lastChanged = 0;
        File f = null;
        for (String s : folders){
            File file = new File(s + "/collection.media");

            if (file.lastModified()> lastChanged){
                f = file;
            }
        }

        return f;
    }



    public static boolean SAVE_FILE(File file, Phrase phrase){

        boolean writeSusces = true;



            try (FileOutputStream stream = new FileOutputStream(file)) {
                try {
                    stream.write(phrase.getByteSound());
                } catch (IOException e) {
                    e.printStackTrace();
                    writeSusces = false;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
                writeSusces = false;
            } catch (IOException e) {
                e.printStackTrace();
                writeSusces = false;
            }


        return writeSusces;
    }




}
