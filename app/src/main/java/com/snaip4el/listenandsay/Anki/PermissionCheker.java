package com.snaip4el.listenandsay.Anki;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import com.ichi2.anki.FlashCardsContract;
import com.snaip4el.listenandsay.R;

public class PermissionCheker {

    static final int PERMISSION_ALL = 1;

    static String[] PERMISSIONS = {
            Manifest.permission.RECORD_AUDIO,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            FlashCardsContract.READ_WRITE_PERMISSION
    };

    class PermissionChekerResult{
        final boolean havePermissions;
        final boolean ankiInstaled;

        public PermissionChekerResult(boolean havePermissions, boolean ankiInstaled) {
            this.havePermissions = havePermissions;
            this.ankiInstaled = ankiInstaled;
        }
    }

    public static void CHECK_PERMISSION(Fragment fragment){



        // Here, thisActivity is the current activity
        if(!hasPermissions(fragment.getContext(), PERMISSIONS)){
            fragment.requestPermissions( PERMISSIONS, PERMISSION_ALL);

        }else {
            // хардкод если не спрашиваем разрешения значит они есть, тогда проверяем установлена ли анки.
            AnkiAddingStats ankiAddingStats = checkCanWeAddToAnki(fragment.getContext());
            if(!ankiAddingStats.isCanAddToAnki()){
                Toast toast = Toast.makeText(fragment.getContext(), ankiAddingStats.getMassageToUser(), Toast.LENGTH_LONG);
                toast.show();
            }
        }

    }

    public static boolean CHECK_ANKI(Fragment fragment){
        // хардкод если не спрашиваем разрешения значит они есть, тогда проверяем установлена ли анки.
        AnkiAddingStats ankiAddingStats = checkCanWeAddToAnki(fragment.getContext());
        if(!ankiAddingStats.isCanAddToAnki()){
            Toast toast = Toast.makeText(fragment.getContext(), ankiAddingStats.getMassageToUser(), Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
        return true;
    }


    public static void CHECK_ASK_PERMISSION(Fragment fragment){


        // Here, thisActivity is the current activity
        if(!hasPermissions(fragment.getContext(), PERMISSIONS)){
            fragment.requestPermissions( PERMISSIONS, PERMISSION_ALL);

        }else {

        }

    }




    public static boolean CHECK(Fragment fragment){



        return hasPermissions(fragment.getContext(), PERMISSIONS);
    }



    private static boolean hasPermissions(Context context, String... permissions) {
        if (context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {

                    return false;

                }
            }
        }
        return true;
    }

    private static AnkiAddingStats checkCanWeAddToAnki(Context context){

        String message = "";
        boolean b = true;

        if(!isFilePermissionGranted(context)){
            message = message + context.getString(R.string.do_not_have_file_permission) + " \n";
            b = false;
        }

        if(!isPermissionAnkiGranted(context)){
            message = message + context.getString(R.string.do_not_have_anki_permission) + " \n";
            b = false;
        }

        if (!AnkiDroidHelper.isApiAvailable(context)){
            message = message + context.getString(R.string.instal_anki) + " \n";
            b = false;
        }


        if(message == ""){
            message = context.getString(R.string.import_enabled);
        }

        return new AnkiAddingStats(b, message);

    }

    private static boolean isFilePermissionGranted(Context context){
        boolean b = false;
        if (ContextCompat.checkSelfPermission(context , Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            b = true;
        }
        return b;
    }

    private static boolean isPermissionAnkiGranted(Context context){
        boolean b = false;
        if (!AnkiSender.getInstens(context).getmAnkiDroid().shouldRequestPermission()) {
            b = true;
        }
        return b;
    }

}
