package com.snaip4el.listenandsay.Anki;

import android.content.Context;

import com.snaip4el.listenandsay.NamesSR;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

/** Some fields to store configuration details for AnkiDroid **/
final class AnkiDroidConfig {
    // Name of deck which will be created in AnkiDroid
    private static final String DECK_NAME = "Tatoeba";
    public static String getDeskName(Context context){
        String deskName = DECK_NAME;
        deskName = deskName +" "+ NamesSR.getLerningLanguage(context);
        return deskName;
    }
    // Name of model which will be created in AnkiDroid тип записи(как выглядит колода)
    public static final String MODEL_NAME = "TatoebaTipe";
    // Optional space separated list of tags to add to every note
    public static final Set<String> TAGS = new HashSet<>(Collections.singletonList("API_Sample_App"));
    // List of field names that will be used in AnkiDroid model
    // поля которы добавляем
    public static final String[] FIELDS = {"Sound","Phrase","Translation"};
    // List of card names that will be used in AnkiDroid (one for each direction of learning)
    public static final String[] CARD_NAMES = {"Listning"};
    // CSS to share between all the cards (optional). User will need to install the NotoSans font by themselves
    public static final String CSS = ".card {\n" +
            " font-family: NotoSansJP;\n" +
            " font-size: 24px;\n" +
            " text-align: center;\n" +
            " color: black;\n" +
            " background-color: white;\n" +
            " word-wrap: break-word;\n" +
            "}\n" +
            "@font-face { font-family: \"NotoSansJP\"; src: url('_NotoSansJP-Regular.otf'); }\n" +
            "@font-face { font-family: \"NotoSansJP\"; src: url('_NotoSansJP-Bold.otf'); font-weight: bold; }\n" +
            "\n" +
            ".big { font-size: 48px; }\n" +
            ".small { font-size: 18px;}\n";
    // Template for the question of each card
    static final String QFMT1 = "<div style='font-family: Arial; font-size: 20px;'>{{Sound}}</div>";
    public static final String[] QFMT = {QFMT1,};
    // Template for the answer (use identical for both sides)
    static final String AFMT1 ="<div style='font-family: Arial; font-size: 20px;'>{{Phrase}}</div>\n" +
            "<hr id=answer>\n" +
            "<div style='font-family: Arial; font-size: 20px;'>{{Translation}}</div>\n";

    public static final String[] AFMT = {AFMT1};
    // Define two keys which will be used when using legacy ACTION_SEND intent
    public static final String FRONT_SIDE_KEY = FIELDS[0];
    public static final String BACK_SIDE_KEY = FIELDS[2];


}
