package com.snaip4el.listenandsay.ConvertersForDB;

import androidx.room.TypeConverter;

import com.snaip4el.listenandsay.TasksResults.ListeningResult;

public class ListeningResultConverter {

    @TypeConverter
    public int fromListeningResult(ListeningResult listeningRsult) {
        if (listeningRsult == null) return ListeningResult.DID_NOT_PRESS.getNumInBas();
        return listeningRsult.getNumInBas();
    }

    @TypeConverter
    public ListeningResult toListeningResult(int i) {

        ListeningResult listeningResult = ListeningResult.DID_NOT_PRESS;
        if (i == 1) return ListeningResult.PRESSED_YES;
        if (i == 2) return ListeningResult.PRESSED_NO;
        return listeningResult;
    }


}
