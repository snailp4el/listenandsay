package com.snaip4el.listenandsay.ConvertersForDB;

import androidx.room.TypeConverter;
import com.snaip4el.listenandsay.TasksResults.SpeakingResult;

public class SpeakingResultConverter {

    @TypeConverter
    public int fromSpeakingResult(SpeakingResult speakingResult) {
        if (speakingResult == null) return SpeakingResult.DID_NOT_SAY.getNumInBas();
        return speakingResult.getNumInBas();
    }

    @TypeConverter
    public SpeakingResult toSpeakingResult(int i) {

        SpeakingResult speakingResult = SpeakingResult.DID_NOT_SAY;

        if (i == SpeakingResult.SAID_OK.getNumInBas()) return SpeakingResult.SAID_OK;
        if (i == SpeakingResult.SAID_WRONG.getNumInBas()) return SpeakingResult.SAID_WRONG;
        if (i == SpeakingResult.SAID_OK_FIRST_TRY.getNumInBas()) return SpeakingResult.SAID_OK_FIRST_TRY;

        return speakingResult;
    }


}
