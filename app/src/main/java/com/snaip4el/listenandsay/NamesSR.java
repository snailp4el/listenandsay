package com.snaip4el.listenandsay;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;

import org.jetbrains.annotations.Nullable;

import java.util.Locale;

public class NamesSR {

    public static final String FILE = "usersSetting";
    public static final String NATIVE_LANGUAGE = "nativeLanguage";
    public static final String LEARNING_LANGUAGE = "LearningLanguage";
    public static final String LEARNING_LANGUAGE_FLAG = "LearningLanguageFlag";

    public static final String MIN_PHRASES_IN_BASE = "MinPhraseInBase";
    public static final int minPhraseInBase = 500;//todo make more
    public static final String EXPORT_TO_ANKI = "exportToAnki";




    public static class ListeningTask{

        private ListeningTask() {
        }


        public static final String AUTO_YES_IF_BUTTON_DONT_PRESED = "autoYes";


        public static Boolean isAutoYesIfButtonDontPresed(Context context){
            SharedPreferences mSettings = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE);
            return mSettings.getBoolean(ListeningTask.AUTO_YES_IF_BUTTON_DONT_PRESED, true);
        }

        public static void setAutoYesIfButtonDontPresed(Context context, Boolean b){
            SharedPreferences.Editor editor = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE).edit();
            editor.putBoolean(ListeningTask.AUTO_YES_IF_BUTTON_DONT_PRESED, b);
            editor.apply();
        }
    }



    private NamesSR() {
    }

    public static Boolean isExportToAnki(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE);
        return mSettings.getBoolean(EXPORT_TO_ANKI, false);
    }

    public static void setExportToAnki(Context context, Boolean b){
        SharedPreferences.Editor editor = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE).edit();
        editor.putBoolean(EXPORT_TO_ANKI, b);
        editor.apply();
    }


    public static int getMinPhrasesInBase(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE);
        return  mSettings.getInt(NamesSR.MIN_PHRASES_IN_BASE, minPhraseInBase);
    }

    public static String getLerningLanguage(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE);
        return mSettings.getString(NamesSR.LEARNING_LANGUAGE, null);
    }

    public static void setLerningLanguage(@NonNull Context context, String lerningLanguage){
        SharedPreferences.Editor editor = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE).edit();
        editor.putString(NamesSR.LEARNING_LANGUAGE, lerningLanguage);
        editor.apply();

    }

    //todo
    public static String getLerningLanguageFlag(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE);
        return mSettings.getString(NamesSR.LEARNING_LANGUAGE_FLAG, null);
    }

    public static void setLerningLanguageFlag(@NonNull Context context, String lerningLanguageFlag){
        SharedPreferences.Editor editor = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE).edit();
        editor.putString(NamesSR.LEARNING_LANGUAGE_FLAG, lerningLanguageFlag);
        editor.apply();

    }


    public static String getNativeLanguage(Context context){
        SharedPreferences mSettings = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE);
        return mSettings.getString(NamesSR.NATIVE_LANGUAGE, null);
    }

    public static void setNativeLanguage(Context context, String nativeLanguage){
        SharedPreferences.Editor editor = context.getSharedPreferences(NamesSR.FILE, Context.MODE_PRIVATE).edit();
        editor.putString(NamesSR.NATIVE_LANGUAGE, nativeLanguage);
        editor.apply();

    }

    @Nullable
    public static Drawable GET_FLAG(Context context){
        String saveFlag = NamesSR.getLerningLanguageFlag(context);
        if (saveFlag != null){
            Drawable d = new BitmapDrawable(context.getResources(), TatoebaFetcher.CONVERTsTRINGtObITMAP(saveFlag));
            return  d;
        }
        return null;
    }

/*    eng English | spa Spanish | kab Kabyle | deu German |
    por Portuguese | fra French | hun Hungarian | rus Russian |
    ber Berber | epo Esperanto | fin Finnish | wuu Shanghainese | nld Dutch |
    cmn Mandarin Chinese | jpn Japanese | heb Hebrew | lat Latin | dtp Central Dusun |
    mar Marathi | ukr Ukrainian | kor Korean | pol Polish | tha Thai | cat Catalan |
    cbk Chavacano | ron Romanian | tur Turkish | nst Naga (Tangshang) | shy Tachawit |*/

    public static Locale GET_LEARNING_LANGUAGE_LOCALE(Context context){


        switch (getLerningLanguage(context)){
            case "eng": return Locale.US;
            case "spa": return new Locale("es", "ES");
            case "kab": return new Locale("kab", "");
            case "deu": return new Locale("de", "DE");
            case "por": return new Locale("pt", "PT");
            case "fra": return new Locale("fr", "FR");
            case "hun": return new Locale("hu", "HU");
            case "rus": return new Locale("ru", "RU");
            //case "ber": return new Locale("", ""); //can't found
            case "epo": return new Locale("eo", "");
            case "fin": return new Locale("fi", "FI");
            //case "wuu": return new Locale("", ""); //can't found
            case "nld": return new Locale("nl", "");
            case "cmn": return new Locale("zh", "");
            case "jpn": return new Locale("ja", "JP");
            case "heb": return new Locale("iw", "IL");
            //case "lat": return new Locale("", "");//can't found
            //case "dtp": return new Locale("eo", "");//can't found
            //case "mar": return new Locale("eo", "");
            case "ukr": return new Locale("uk", "UA");
            case "kor": return new Locale("ko", "KR");
            case "pol": return new Locale("pl", "PL");
            case "tha": return new Locale("th", "TH");
            case "cat": return new Locale("ca", "ES");
            //case "cbk": return new Locale("eo", "");
            case "ron": return new Locale("ro", "RO");
            case "tur": return new Locale("tr", "TR");
            //case "nst": return new Locale("", "");
            //case "shy": return new Locale("", "");



            default: return Locale.US;
        }
    }

}
